﻿namespace Microservices.IoT.Client.WinForms
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.settingsTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.settingsUC1 = new Microservices.IoT.Client.WinForms.UserControls.Settings.SettingsUC();
            this.eventTypesTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.eventTypesUC = new Microservices.IoT.Client.WinForms.UserControls.EventTypesUC();
            this.foodTypesTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.foodTypesUC = new Microservices.IoT.Client.WinForms.UserControls.FoodTypesUC();
            this.foodTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.foodUC = new Microservices.IoT.Client.WinForms.UserControls.FoodUC();
            this.fridgesTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.fridgesUC = new Microservices.IoT.Client.WinForms.UserControls.FridgesUC();
            this.fridgesAndEventsTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.fridgesAndEventsUC = new Microservices.IoT.Client.WinForms.UserControls.FridgesAndEventsUC();
            this.trafficSensorMeasurementsPage = new DevExpress.XtraTab.XtraTabPage();
            this.trafficSensorsMeasurementsUC1 = new Microservices.IoT.Client.WinForms.UserControls.Sensors.Traffic.TrafficSensorsMeasurementsUC();
            this.trafficSensorsPage = new DevExpress.XtraTab.XtraTabPage();
            this.trafficSensorsUC1 = new Microservices.IoT.Client.WinForms.UserControls.Sensors.Traffic.TrafficSensorsUC();
            this.temperatureSensorsTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.temperatureSensorsUC = new Microservices.IoT.Client.WinForms.UserControls.TemperatureSensorsUC();
            this.fridgeSensorsTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.fridgeSensorsUC1 = new Microservices.IoT.Client.WinForms.UserControls.FridgeSensorsUC();
            this.mainTabControl = new DevExpress.XtraTab.XtraTabControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.settingsTabPage.SuspendLayout();
            this.eventTypesTabPage.SuspendLayout();
            this.foodTypesTabPage.SuspendLayout();
            this.foodTabPage.SuspendLayout();
            this.fridgesTabPage.SuspendLayout();
            this.fridgesAndEventsTabPage.SuspendLayout();
            this.trafficSensorMeasurementsPage.SuspendLayout();
            this.trafficSensorsPage.SuspendLayout();
            this.temperatureSensorsTabPage.SuspendLayout();
            this.fridgeSensorsTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainTabControl)).BeginInit();
            this.mainTabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(840, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 561);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(840, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 561);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(840, 0);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 561);
            // 
            // settingsTabPage
            // 
            this.settingsTabPage.Controls.Add(this.settingsUC1);
            this.settingsTabPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.settingsTabPage.Name = "settingsTabPage";
            this.settingsTabPage.Size = new System.Drawing.Size(838, 530);
            this.settingsTabPage.Text = "Settings";
            // 
            // settingsUC1
            // 
            this.settingsUC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settingsUC1.Location = new System.Drawing.Point(0, 0);
            this.settingsUC1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.settingsUC1.Name = "settingsUC1";
            this.settingsUC1.Size = new System.Drawing.Size(838, 530);
            this.settingsUC1.TabIndex = 0;
            // 
            // eventTypesTabPage
            // 
            this.eventTypesTabPage.Controls.Add(this.eventTypesUC);
            this.eventTypesTabPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.eventTypesTabPage.Name = "eventTypesTabPage";
            this.eventTypesTabPage.Size = new System.Drawing.Size(838, 530);
            this.eventTypesTabPage.Text = "Event types";
            // 
            // eventTypesUC
            // 
            this.eventTypesUC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventTypesUC.Location = new System.Drawing.Point(0, 0);
            this.eventTypesUC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.eventTypesUC.Name = "eventTypesUC";
            this.eventTypesUC.Size = new System.Drawing.Size(838, 530);
            this.eventTypesUC.TabIndex = 0;
            // 
            // foodTypesTabPage
            // 
            this.foodTypesTabPage.Controls.Add(this.foodTypesUC);
            this.foodTypesTabPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.foodTypesTabPage.Name = "foodTypesTabPage";
            this.foodTypesTabPage.Size = new System.Drawing.Size(838, 530);
            this.foodTypesTabPage.Text = "Food types";
            // 
            // foodTypesUC
            // 
            this.foodTypesUC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.foodTypesUC.Location = new System.Drawing.Point(0, 0);
            this.foodTypesUC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.foodTypesUC.Name = "foodTypesUC";
            this.foodTypesUC.Size = new System.Drawing.Size(838, 530);
            this.foodTypesUC.TabIndex = 0;
            // 
            // foodTabPage
            // 
            this.foodTabPage.Controls.Add(this.foodUC);
            this.foodTabPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.foodTabPage.Name = "foodTabPage";
            this.foodTabPage.Size = new System.Drawing.Size(838, 530);
            this.foodTabPage.Text = "Food";
            // 
            // foodUC
            // 
            this.foodUC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.foodUC.Location = new System.Drawing.Point(0, 0);
            this.foodUC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.foodUC.Name = "foodUC";
            this.foodUC.Size = new System.Drawing.Size(838, 530);
            this.foodUC.TabIndex = 0;
            // 
            // fridgesTabPage
            // 
            this.fridgesTabPage.Controls.Add(this.fridgesUC);
            this.fridgesTabPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fridgesTabPage.Name = "fridgesTabPage";
            this.fridgesTabPage.Size = new System.Drawing.Size(838, 530);
            this.fridgesTabPage.Text = "Fridges";
            // 
            // fridgesUC
            // 
            this.fridgesUC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fridgesUC.Location = new System.Drawing.Point(0, 0);
            this.fridgesUC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fridgesUC.Name = "fridgesUC";
            this.fridgesUC.Size = new System.Drawing.Size(838, 530);
            this.fridgesUC.TabIndex = 0;
            // 
            // fridgesAndEventsTabPage
            // 
            this.fridgesAndEventsTabPage.Controls.Add(this.fridgesAndEventsUC);
            this.fridgesAndEventsTabPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fridgesAndEventsTabPage.Name = "fridgesAndEventsTabPage";
            this.fridgesAndEventsTabPage.Size = new System.Drawing.Size(838, 530);
            this.fridgesAndEventsTabPage.Text = "Fridges and events";
            // 
            // fridgesAndEventsUC
            // 
            this.fridgesAndEventsUC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fridgesAndEventsUC.Location = new System.Drawing.Point(0, 0);
            this.fridgesAndEventsUC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fridgesAndEventsUC.Name = "fridgesAndEventsUC";
            this.fridgesAndEventsUC.Size = new System.Drawing.Size(838, 530);
            this.fridgesAndEventsUC.TabIndex = 0;
            // 
            // trafficSensorMeasurementsPage
            // 
            this.trafficSensorMeasurementsPage.Controls.Add(this.trafficSensorsMeasurementsUC1);
            this.trafficSensorMeasurementsPage.Name = "trafficSensorMeasurementsPage";
            this.trafficSensorMeasurementsPage.Size = new System.Drawing.Size(838, 530);
            this.trafficSensorMeasurementsPage.Text = "Traffic sensor measurements";
            // 
            // trafficSensorsMeasurementsUC1
            // 
            this.trafficSensorsMeasurementsUC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trafficSensorsMeasurementsUC1.Location = new System.Drawing.Point(0, 0);
            this.trafficSensorsMeasurementsUC1.Name = "trafficSensorsMeasurementsUC1";
            this.trafficSensorsMeasurementsUC1.Size = new System.Drawing.Size(838, 530);
            this.trafficSensorsMeasurementsUC1.TabIndex = 0;
            // 
            // trafficSensorsPage
            // 
            this.trafficSensorsPage.Controls.Add(this.trafficSensorsUC1);
            this.trafficSensorsPage.Name = "trafficSensorsPage";
            this.trafficSensorsPage.Size = new System.Drawing.Size(838, 530);
            this.trafficSensorsPage.Text = "Traffic Sensors";
            // 
            // trafficSensorsUC1
            // 
            this.trafficSensorsUC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trafficSensorsUC1.Location = new System.Drawing.Point(0, 0);
            this.trafficSensorsUC1.Name = "trafficSensorsUC1";
            this.trafficSensorsUC1.Size = new System.Drawing.Size(838, 530);
            this.trafficSensorsUC1.TabIndex = 0;
            // 
            // temperatureSensorsTabPage
            // 
            this.temperatureSensorsTabPage.Controls.Add(this.temperatureSensorsUC);
            this.temperatureSensorsTabPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.temperatureSensorsTabPage.Name = "temperatureSensorsTabPage";
            this.temperatureSensorsTabPage.Size = new System.Drawing.Size(838, 530);
            this.temperatureSensorsTabPage.Text = "Temperature Sensors";
            // 
            // temperatureSensorsUC
            // 
            this.temperatureSensorsUC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.temperatureSensorsUC.Location = new System.Drawing.Point(0, 0);
            this.temperatureSensorsUC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.temperatureSensorsUC.Name = "temperatureSensorsUC";
            this.temperatureSensorsUC.Size = new System.Drawing.Size(838, 530);
            this.temperatureSensorsUC.TabIndex = 0;
            // 
            // fridgeSensorsTabPage
            // 
            this.fridgeSensorsTabPage.Controls.Add(this.fridgeSensorsUC1);
            this.fridgeSensorsTabPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fridgeSensorsTabPage.Name = "fridgeSensorsTabPage";
            this.fridgeSensorsTabPage.Size = new System.Drawing.Size(838, 530);
            this.fridgeSensorsTabPage.Text = "Fridge sensors";
            // 
            // fridgeSensorsUC1
            // 
            this.fridgeSensorsUC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fridgeSensorsUC1.Location = new System.Drawing.Point(0, 0);
            this.fridgeSensorsUC1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fridgeSensorsUC1.Name = "fridgeSensorsUC1";
            this.fridgeSensorsUC1.PeriodicUpdatesEnabled = true;
            this.fridgeSensorsUC1.Size = new System.Drawing.Size(838, 530);
            this.fridgeSensorsUC1.TabIndex = 0;
            // 
            // mainTabControl
            // 
            this.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTabControl.Location = new System.Drawing.Point(0, 0);
            this.mainTabControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedTabPage = this.fridgesAndEventsTabPage;
            this.mainTabControl.Size = new System.Drawing.Size(840, 561);
            this.mainTabControl.TabIndex = 11;
            this.mainTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.fridgeSensorsTabPage,
            this.temperatureSensorsTabPage,
            this.trafficSensorsPage,
            this.trafficSensorMeasurementsPage,
            this.fridgesAndEventsTabPage,
            this.fridgesTabPage,
            this.foodTabPage,
            this.foodTypesTabPage,
            this.eventTypesTabPage,
            this.settingsTabPage});
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 561);
            this.Controls.Add(this.mainTabControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.IconOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("MainWindow.IconOptions.LargeImage")));
            this.Name = "MainWindow";
            this.Text = "Fridge sensors and management console";
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.settingsTabPage.ResumeLayout(false);
            this.eventTypesTabPage.ResumeLayout(false);
            this.foodTypesTabPage.ResumeLayout(false);
            this.foodTabPage.ResumeLayout(false);
            this.fridgesTabPage.ResumeLayout(false);
            this.fridgesAndEventsTabPage.ResumeLayout(false);
            this.trafficSensorMeasurementsPage.ResumeLayout(false);
            this.trafficSensorsPage.ResumeLayout(false);
            this.temperatureSensorsTabPage.ResumeLayout(false);
            this.fridgeSensorsTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainTabControl)).EndInit();
            this.mainTabControl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraTab.XtraTabControl mainTabControl;
        private DevExpress.XtraTab.XtraTabPage fridgesAndEventsTabPage;
        private UserControls.FridgesAndEventsUC fridgesAndEventsUC;
        private DevExpress.XtraTab.XtraTabPage fridgeSensorsTabPage;
        private UserControls.FridgeSensorsUC fridgeSensorsUC1;
        private DevExpress.XtraTab.XtraTabPage temperatureSensorsTabPage;
        private UserControls.TemperatureSensorsUC temperatureSensorsUC;
        private DevExpress.XtraTab.XtraTabPage trafficSensorsPage;
        private UserControls.Sensors.Traffic.TrafficSensorsUC trafficSensorsUC1;
        private DevExpress.XtraTab.XtraTabPage trafficSensorMeasurementsPage;
        private UserControls.Sensors.Traffic.TrafficSensorsMeasurementsUC trafficSensorsMeasurementsUC1;
        private DevExpress.XtraTab.XtraTabPage fridgesTabPage;
        private UserControls.FridgesUC fridgesUC;
        private DevExpress.XtraTab.XtraTabPage foodTabPage;
        private UserControls.FoodUC foodUC;
        private DevExpress.XtraTab.XtraTabPage foodTypesTabPage;
        private UserControls.FoodTypesUC foodTypesUC;
        private DevExpress.XtraTab.XtraTabPage eventTypesTabPage;
        private UserControls.EventTypesUC eventTypesUC;
        private DevExpress.XtraTab.XtraTabPage settingsTabPage;
        private UserControls.Settings.SettingsUC settingsUC1;
    }
}

