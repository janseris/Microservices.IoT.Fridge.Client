﻿namespace Microservices.IoT.Client.WinForms.Windows.Sensors.Temperature
{
    partial class AddTemperatureSensorWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.nameLabel = new DevExpress.XtraEditors.LabelControl();
            this.sensorLabel = new DevExpress.XtraEditors.LabelControl();
            this.manufacturerLabel = new DevExpress.XtraEditors.LabelControl();
            this.temperatureLabel = new DevExpress.XtraEditors.LabelControl();
            this.openPortsCountLabel = new DevExpress.XtraEditors.LabelControl();
            this.submitButton = new DevExpress.XtraEditors.SimpleButton();
            this.cancelButton = new DevExpress.XtraEditors.SimpleButton();
            this.nameTextBox = new DevExpress.XtraEditors.TextEdit();
            this.sensorTypeComboBox = new DevExpress.XtraEditors.ComboBoxEdit();
            this.manufacturerComboBox = new DevExpress.XtraEditors.ComboBoxEdit();
            this.temperatureNumberEdit = new DevExpress.XtraEditors.SpinEdit();
            this.openPortsNumberEdit = new DevExpress.XtraEditors.SpinEdit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nameTextBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorTypeComboBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.manufacturerComboBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureNumberEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.openPortsNumberEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.manufacturerLabel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.sensorLabel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.nameLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.nameTextBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.sensorTypeComboBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.manufacturerComboBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.temperatureLabel, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.openPortsCountLabel, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.submitButton, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.cancelButton, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.temperatureNumberEdit, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.openPortsNumberEdit, 1, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66666F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66666F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(478, 326);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // nameLabel
            // 
            this.nameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nameLabel.Location = new System.Drawing.Point(3, 3);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(233, 48);
            this.nameLabel.TabIndex = 0;
            this.nameLabel.Text = "Name";
            // 
            // sensorLabel
            // 
            this.sensorLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorLabel.Location = new System.Drawing.Point(3, 57);
            this.sensorLabel.Name = "sensorLabel";
            this.sensorLabel.Size = new System.Drawing.Size(233, 48);
            this.sensorLabel.TabIndex = 2;
            this.sensorLabel.Text = "Sensor type";
            // 
            // manufacturerLabel
            // 
            this.manufacturerLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.manufacturerLabel.Location = new System.Drawing.Point(3, 111);
            this.manufacturerLabel.Name = "manufacturerLabel";
            this.manufacturerLabel.Size = new System.Drawing.Size(233, 48);
            this.manufacturerLabel.TabIndex = 4;
            this.manufacturerLabel.Text = "Manufacturer";
            // 
            // temperatureLabel
            // 
            this.temperatureLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.temperatureLabel.Location = new System.Drawing.Point(3, 165);
            this.temperatureLabel.Name = "temperatureLabel";
            this.temperatureLabel.Size = new System.Drawing.Size(233, 48);
            this.temperatureLabel.TabIndex = 8;
            this.temperatureLabel.Text = "Temperature";
            // 
            // openPortsCountLabel
            // 
            this.openPortsCountLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.openPortsCountLabel.Location = new System.Drawing.Point(3, 219);
            this.openPortsCountLabel.Name = "openPortsCountLabel";
            this.openPortsCountLabel.Size = new System.Drawing.Size(233, 48);
            this.openPortsCountLabel.TabIndex = 10;
            this.openPortsCountLabel.Text = "Open ports count";
            // 
            // submitButton
            // 
            this.submitButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.submitButton.Location = new System.Drawing.Point(3, 273);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(233, 50);
            this.submitButton.TabIndex = 11;
            this.submitButton.Text = "OK";
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cancelButton.Location = new System.Drawing.Point(242, 273);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(233, 50);
            this.cancelButton.TabIndex = 12;
            this.cancelButton.Text = "Cancel";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.nameTextBox.Location = new System.Drawing.Point(242, 13);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(233, 28);
            this.nameTextBox.TabIndex = 13;
            this.nameTextBox.EditValueChanged += new System.EventHandler(this.nameTextBox_EditValueChanged);
            // 
            // sensorTypeComboBox
            // 
            this.sensorTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorTypeComboBox.Location = new System.Drawing.Point(242, 67);
            this.sensorTypeComboBox.Name = "sensorTypeComboBox";
            this.sensorTypeComboBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sensorTypeComboBox.Size = new System.Drawing.Size(233, 28);
            this.sensorTypeComboBox.TabIndex = 14;
            this.sensorTypeComboBox.EditValueChanged += new System.EventHandler(this.sensorTypeComboBox_EditValueChanged);
            // 
            // manufacturerComboBox
            // 
            this.manufacturerComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.manufacturerComboBox.Location = new System.Drawing.Point(242, 121);
            this.manufacturerComboBox.Name = "manufacturerComboBox";
            this.manufacturerComboBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.manufacturerComboBox.Size = new System.Drawing.Size(233, 28);
            this.manufacturerComboBox.TabIndex = 15;
            this.manufacturerComboBox.EditValueChanged += new System.EventHandler(this.manufacturerComboBox_EditValueChanged);
            // 
            // temperatureNumberEdit
            // 
            this.temperatureNumberEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.temperatureNumberEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.temperatureNumberEdit.Location = new System.Drawing.Point(242, 175);
            this.temperatureNumberEdit.Name = "temperatureNumberEdit";
            this.temperatureNumberEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.temperatureNumberEdit.Size = new System.Drawing.Size(233, 28);
            this.temperatureNumberEdit.TabIndex = 16;
            this.temperatureNumberEdit.EditValueChanged += new System.EventHandler(this.temperatureNumberEdit_EditValueChanged);
            // 
            // openPortsNumberEdit
            // 
            this.openPortsNumberEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.openPortsNumberEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.openPortsNumberEdit.Location = new System.Drawing.Point(242, 229);
            this.openPortsNumberEdit.Name = "openPortsNumberEdit";
            this.openPortsNumberEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.openPortsNumberEdit.Properties.IsFloatValue = false;
            this.openPortsNumberEdit.Properties.MaskSettings.Set("mask", "N00");
            this.openPortsNumberEdit.Size = new System.Drawing.Size(233, 28);
            this.openPortsNumberEdit.TabIndex = 17;
            this.openPortsNumberEdit.EditValueChanged += new System.EventHandler(this.openPortsNumberEdit_EditValueChanged);
            // 
            // AddTemperatureSensorWindow
            // 
            this.AcceptButton = this.submitButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(478, 326);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "AddTemperatureSensorWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddTemperatureSensorWindow";
            this.Load += new System.EventHandler(this.AddTemperatureSensorWindow_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nameTextBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorTypeComboBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.manufacturerComboBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureNumberEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.openPortsNumberEdit.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.LabelControl openPortsCountLabel;
        private DevExpress.XtraEditors.LabelControl temperatureLabel;
        private DevExpress.XtraEditors.LabelControl manufacturerLabel;
        private DevExpress.XtraEditors.LabelControl sensorLabel;
        private DevExpress.XtraEditors.LabelControl nameLabel;
        private DevExpress.XtraEditors.SimpleButton submitButton;
        private DevExpress.XtraEditors.SimpleButton cancelButton;
        private DevExpress.XtraEditors.TextEdit nameTextBox;
        private DevExpress.XtraEditors.ComboBoxEdit sensorTypeComboBox;
        private DevExpress.XtraEditors.ComboBoxEdit manufacturerComboBox;
        private DevExpress.XtraEditors.SpinEdit temperatureNumberEdit;
        private DevExpress.XtraEditors.SpinEdit openPortsNumberEdit;
    }
}