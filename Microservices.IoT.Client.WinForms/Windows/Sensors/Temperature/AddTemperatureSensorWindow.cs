﻿using System;
using System.Linq;

using DevExpress.XtraEditors;

using Microservices.IoT.Client.WinForms.APIClientFactories;
using Microservices.IoT.Client.WinForms.Models.Sensors.Temperature;
using Microservices.IoT.Sensors.Client.Temperature;

using Microsoft.AspNetCore.Http;

namespace Microservices.IoT.Client.WinForms.Windows.Sensors.Temperature
{
    public partial class AddTemperatureSensorWindow : DevExpress.XtraEditors.XtraForm
    {
        public AddTemperatureSensorWindow()
        {
            InitializeComponent();
        }

        private TemperatureSensorsClient Service => TemperatureSensorClientFactory.GetClient();

        private async void AddTemperatureSensorWindow_Load(object sender, EventArgs e)
        {
            //this.IconOptions.Icon = (this.ParentForm as XtraForm).IconOptions.Icon;
            var manufacturers = await Service.GetAllManufacturersAsync();
            var types = await Service.GetAllTempSensorTypesAsync();
            manufacturerComboBox.Properties.Items.AddRange(manufacturers.ToList());
            sensorTypeComboBox.Properties.Items.AddRange(types.ToList());
        }

        public TemperatureSensorInsertModel Data { get; set; } = new TemperatureSensorInsertModel();

        private void nameTextBox_EditValueChanged(object sender, EventArgs e)
        {
            Data.Name = nameTextBox.Text;
        }

        private void sensorTypeComboBox_EditValueChanged(object sender, EventArgs e)
        {
            var comboBox = sender as ComboBoxEdit;
            Data.TypeID = (comboBox.EditValue as TempSensorType).TempSensorTypeId;
        }

        private void manufacturerComboBox_EditValueChanged(object sender, EventArgs e)
        {
            var comboBox = sender as ComboBoxEdit;
            Data.TypeID = (comboBox.EditValue as Manufacturer).ManufacturerId;
        }

        private void temperatureNumberEdit_EditValueChanged(object sender, EventArgs e)
        {
            Data.Temperature = (double)temperatureNumberEdit.Value;
        }

        private void openPortsNumberEdit_EditValueChanged(object sender, EventArgs e)
        {
            Data.OpenPortsCount = (int)openPortsNumberEdit.Value;
        }

        private async void submitButton_Click(object sender, EventArgs e)
        {
            var apiData = new TempSensor()
            {
                CreationDateTime = DateTime.Now,
                LastTimeChecked = DateTime.Now,
                Name = Data.Name,
                NumberOpenPorts = Data.OpenPortsCount,
                TempSensorManufacturerId = Data.ManufacturerID,
                TempSensorTypeId = Data.TypeID,
                TempValue = Data.Temperature
            };

            Response8 response = null;
            try
            {
                //not working for unknown reason -> problem with ID on insert
                response = await Service.PostCreateNewTempSensorAsync(apiData);
            } catch (Microservices.IoT.Sensors.Client.Temperature.ApiException ex)
            {
                if (ex.StatusCode == StatusCodes.Status400BadRequest)
                {
                    XtraMessageBox.Show(ex.Message, "Error creating new temperature sensor");
                    return;
                }
            }
            XtraMessageBox.Show($"New temperature sensor created: ID {response.TempSensorId}", "Success");
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }
    }
}
