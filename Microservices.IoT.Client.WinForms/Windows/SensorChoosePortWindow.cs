﻿using System.Windows.Forms;

namespace Microservices.IoT.Client.WinForms.Windows
{
    public partial class SensorChoosePortWindow : Form
    {
        public SensorChoosePortWindow()
        {
            InitializeComponent();
        }

        public int Port { get => int.Parse(portTextEdit.EditValue as string); }
    }
}
