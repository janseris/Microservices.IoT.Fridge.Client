﻿namespace Microservices.IoT.Client.WinForms.Windows
{
    partial class SensorChoosePortWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SensorChoosePortWindow));
            this.portLabel = new DevExpress.XtraEditors.LabelControl();
            this.portTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.okButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.portTextEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // portLabel
            // 
            this.portLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.portLabel.Appearance.Options.UseFont = true;
            this.portLabel.Location = new System.Drawing.Point(47, 25);
            this.portLabel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.portLabel.Name = "portLabel";
            this.portLabel.Size = new System.Drawing.Size(23, 16);
            this.portLabel.TabIndex = 0;
            this.portLabel.Text = "Port";
            // 
            // portTextEdit
            // 
            this.portTextEdit.Location = new System.Drawing.Point(96, 22);
            this.portTextEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.portTextEdit.Name = "portTextEdit";
            this.portTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.portTextEdit.Properties.Appearance.Options.UseFont = true;
            this.portTextEdit.Size = new System.Drawing.Size(80, 22);
            this.portTextEdit.TabIndex = 1;
            // 
            // okButton
            // 
            this.okButton.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.okButton.Appearance.Options.UseFont = true;
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.okButton.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.okButton.Location = new System.Drawing.Point(47, 51);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(129, 32);
            this.okButton.TabIndex = 2;
            this.okButton.Text = "OK";
            // 
            // SensorChoosePortWindow
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(218, 103);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.portTextEdit);
            this.Controls.Add(this.portLabel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "SensorChoosePortWindow";
            this.Text = "Choose port";
            ((System.ComponentModel.ISupportInitialize)(this.portTextEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl portLabel;
        private DevExpress.XtraEditors.TextEdit portTextEdit;
        private DevExpress.XtraEditors.SimpleButton okButton;
    }
}