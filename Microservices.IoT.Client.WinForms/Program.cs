﻿using System;
using System.Threading;
using System.Windows.Forms;

using DevExpress.XtraEditors;

namespace Microservices.IoT.Client.WinForms
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            WindowsFormsSettings.DefaultFont = new System.Drawing.Font("Segoe UI", 12);
            WindowsFormsSettings.DefaultMenuFont = new System.Drawing.Font("Segoe UI", 12);

            //Register an exception handler for UI thread(s)
            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);

            //Register an exception handler for non-UI threads
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            Application.Run(new MainWindow());
        }

        /// <summary>
        /// Exception handler for UI thread(s)
        /// </summary>
        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            ShowExceptionDetails(e.Exception);
        }

        /// <summary>
        /// Exception handler for non-UI threads
        /// </summary>
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            ShowExceptionDetails(e.ExceptionObject as Exception);
        }

        private static void ShowExceptionDetails(Exception Ex)
        {
            // Do logging of exception details
            XtraMessageBox.Show(Ex.Message, "Unexpected error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
