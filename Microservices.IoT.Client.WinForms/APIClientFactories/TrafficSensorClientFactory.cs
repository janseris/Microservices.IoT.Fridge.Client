﻿using System;
using System.Net.Http;

using Microservices.IoT.Sensors.Client.Traffic;

namespace Microservices.IoT.Client.WinForms.APIClientFactories
{
    public class TrafficSensorClientFactory
    {
        public static TrafficSensorsClient GetClient()
        {
            var baseURL = AppState.TrafficSensorsAPIBaseURL;
            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(baseURL);
            return new TrafficSensorsClient(baseURL, httpClient);
        }
    }
}
