﻿using System;
using System.Net.Http;

namespace Microservices.IoT.Client.WinForms.APIClientFactories
{
    public class FridgeSensorsManagementClientFactory
    {
        public static ManagementClient Client => GetClient();
        private static ManagementClient GetClient()
        {
            var baseURL = AppState.ManagementConsoleBaseURL;
            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(baseURL);
            return new ManagementClient(baseURL, httpClient);
        }
    }
}
