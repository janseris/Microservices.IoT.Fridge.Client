﻿using System;
using System.Net.Http;

using Microservices.IoT.Sensors.Client.Temperature;

namespace Microservices.IoT.Client.WinForms.APIClientFactories
{
    public class TemperatureSensorClientFactory
    {
        public static TemperatureSensorsClient GetClient()
        {
            var baseURL = AppState.TemperatureSensorsAPIBaseURL;
            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(baseURL);
            return new TemperatureSensorsClient(baseURL, httpClient);
        }
    }
}
