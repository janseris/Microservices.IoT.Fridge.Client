﻿using System;
using System.Net.Http;

using Microservices.IoT.Sensors.Client;

namespace Microservices.IoT.Client.WinForms.APIClientFactories
{
    public class FridgeSensorClientFactory
    {
        public static SensorClient GetClient(int port)
        {
            var baseURL = $"https://localhost:{port}/";
            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(baseURL);
            return new SensorClient(baseURL, httpClient);
        }
    }
}
