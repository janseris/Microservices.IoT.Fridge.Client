﻿namespace Microservices.IoT.Client.WinForms.UserControls
{
    partial class FridgeSensorsUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FridgeSensorsUC));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.initializeButton = new DevExpress.XtraBars.BarLargeButtonItem();
            this.reloadButton = new DevExpress.XtraBars.BarLargeButtonItem();
            this.startButton = new DevExpress.XtraBars.BarLargeButtonItem();
            this.stopButton = new DevExpress.XtraBars.BarLargeButtonItem();
            this.periodicUpdatingSwitch = new DevExpress.XtraBars.BarToggleSwitchItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.grid = new DevExpress.XtraGrid.GridControl();
            this.fridgeSensorUIModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFridgeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPort = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastUpdate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdatePeriodSeconds = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodicUpdatingEnabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRunning = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fridgeSensorUIModelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.reloadButton,
            this.initializeButton,
            this.startButton,
            this.stopButton,
            this.periodicUpdatingSwitch});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 17;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.initializeButton, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.reloadButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.startButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.stopButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.periodicUpdatingSwitch)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // initializeButton
            // 
            this.initializeButton.Caption = "Initialize";
            this.initializeButton.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.initializeButton.Id = 9;
            this.initializeButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("initializeButton.ImageOptions.Image")));
            this.initializeButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("initializeButton.ImageOptions.LargeImage")));
            this.initializeButton.Name = "initializeButton";
            this.initializeButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.initializeButton_ItemClick);
            // 
            // reloadButton
            // 
            this.reloadButton.Caption = "Refresh running info";
            this.reloadButton.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.reloadButton.Id = 1;
            this.reloadButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("reloadButton.ImageOptions.Image")));
            this.reloadButton.Name = "reloadButton";
            this.reloadButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.reloadButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.reloadButton_ItemClick);
            // 
            // startButton
            // 
            this.startButton.Caption = "Start";
            this.startButton.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.startButton.Id = 12;
            this.startButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("startButton.ImageOptions.Image")));
            this.startButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("startButton.ImageOptions.LargeImage")));
            this.startButton.Name = "startButton";
            this.startButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.startButton_ItemClick);
            // 
            // stopButton
            // 
            this.stopButton.Caption = "Stop";
            this.stopButton.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.stopButton.Id = 13;
            this.stopButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("stopButton.ImageOptions.Image")));
            this.stopButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("stopButton.ImageOptions.LargeImage")));
            this.stopButton.Name = "stopButton";
            this.stopButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.stopButton_ItemClick);
            // 
            // periodicUpdatingSwitch
            // 
            this.periodicUpdatingSwitch.Caption = "Periodic updating";
            this.periodicUpdatingSwitch.Id = 16;
            this.periodicUpdatingSwitch.Name = "periodicUpdatingSwitch";
            this.periodicUpdatingSwitch.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.periodicUpdatingSwitch_CheckedChanged);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(949, 55);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 541);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(949, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 55);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 486);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(949, 55);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 486);
            // 
            // grid
            // 
            this.grid.DataSource = this.fridgeSensorUIModelBindingSource;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.Location = new System.Drawing.Point(0, 55);
            this.grid.MainView = this.gridView;
            this.grid.MenuManager = this.barManager1;
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(949, 486);
            this.grid.TabIndex = 4;
            this.grid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // fridgeSensorUIModelBindingSource
            // 
            this.fridgeSensorUIModelBindingSource.DataSource = typeof(Microservices.IoT.Client.WinForms.Models.Sensors.FridgeSensorUIModel);
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFridgeName,
            this.colPort,
            this.colLastUpdate,
            this.colUpdatePeriodSeconds,
            this.colPeriodicUpdatingEnabled,
            this.colRunning});
            this.gridView.GridControl = this.grid;
            this.gridView.Name = "gridView";
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowViewCaption = true;
            this.gridView.ViewCaption = "Fridge sensors";
            this.gridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView_FocusedRowChanged);
            // 
            // colFridgeName
            // 
            this.colFridgeName.FieldName = "FridgeName";
            this.colFridgeName.Name = "colFridgeName";
            this.colFridgeName.Visible = true;
            this.colFridgeName.VisibleIndex = 0;
            this.colFridgeName.Width = 145;
            // 
            // colPort
            // 
            this.colPort.FieldName = "Port";
            this.colPort.Name = "colPort";
            this.colPort.Visible = true;
            this.colPort.VisibleIndex = 1;
            this.colPort.Width = 43;
            // 
            // colLastUpdate
            // 
            this.colLastUpdate.FieldName = "LastUpdate";
            this.colLastUpdate.Name = "colLastUpdate";
            this.colLastUpdate.Visible = true;
            this.colLastUpdate.VisibleIndex = 2;
            this.colLastUpdate.Width = 85;
            // 
            // colUpdatePeriodSeconds
            // 
            this.colUpdatePeriodSeconds.FieldName = "UpdatePeriodSeconds";
            this.colUpdatePeriodSeconds.Name = "colUpdatePeriodSeconds";
            this.colUpdatePeriodSeconds.Visible = true;
            this.colUpdatePeriodSeconds.VisibleIndex = 3;
            this.colUpdatePeriodSeconds.Width = 121;
            // 
            // colPeriodicUpdatingEnabled
            // 
            this.colPeriodicUpdatingEnabled.FieldName = "PeriodicUpdatingEnabled";
            this.colPeriodicUpdatingEnabled.Name = "colPeriodicUpdatingEnabled";
            this.colPeriodicUpdatingEnabled.Visible = true;
            this.colPeriodicUpdatingEnabled.VisibleIndex = 4;
            this.colPeriodicUpdatingEnabled.Width = 109;
            // 
            // colRunning
            // 
            this.colRunning.FieldName = "Running";
            this.colRunning.Name = "colRunning";
            this.colRunning.Visible = true;
            this.colRunning.VisibleIndex = 5;
            this.colRunning.Width = 368;
            // 
            // FridgeSensorsUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grid);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "FridgeSensorsUC";
            this.Size = new System.Drawing.Size(949, 541);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fridgeSensorUIModelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem reloadButton;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraGrid.GridControl grid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private System.Windows.Forms.BindingSource fridgeSensorUIModelBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colFridgeName;
        private DevExpress.XtraGrid.Columns.GridColumn colPort;
        private DevExpress.XtraGrid.Columns.GridColumn colLastUpdate;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdatePeriodSeconds;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodicUpdatingEnabled;
        private DevExpress.XtraGrid.Columns.GridColumn colRunning;
        private DevExpress.XtraBars.BarLargeButtonItem initializeButton;
        private DevExpress.XtraBars.BarLargeButtonItem startButton;
        private DevExpress.XtraBars.BarLargeButtonItem stopButton;
        private DevExpress.XtraBars.BarToggleSwitchItem periodicUpdatingSwitch;
    }
}
