﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using DevExpress.XtraBars;
using DevExpress.XtraEditors;

using Microservices.IoT.Client.WinForms.APIClientFactories;
using Microservices.IoT.Client.WinForms.Models.Sensors;
using Microservices.IoT.Client.WinForms.Windows;

namespace Microservices.IoT.Client.WinForms.UserControls
{
    public partial class FridgeSensorsUC : DevExpress.XtraEditors.XtraUserControl
    {

        public FridgeSensorsUC()
        {
            InitializeComponent();
            InitializePeriodicUpdatesTimer();
            EnablePeriodicUpdates();
        }

        #region periodic updates

        private readonly Timer Timer = new Timer();

        private void InitializePeriodicUpdatesTimer()
        {
            Timer.Interval = 3000;
            Timer.Tick += Timer_Tick;
        }

        private void EnablePeriodicUpdates()
        {
            if(Timer.Enabled == false)
            {
                Timer.Start();
            }
            this.periodicUpdatingSwitch.Checked = true;
        }

        private void DisablePeriodicUpdates()
        {
            if (Timer.Enabled)
            {
                Timer.Stop();
            }
            this.periodicUpdatingSwitch.Checked = false;
        }

        public bool PeriodicUpdatesEnabled
        {
            get => periodicUpdatingSwitch.Checked;
            set  {
                if(value == true)
                {
                    EnablePeriodicUpdates();
                } else
                {
                    DisablePeriodicUpdates();
                }
                periodicUpdatingSwitch.Checked = value;
            }
        }

        #endregion

        private bool Initialized = false;

        private FridgeSensorUIModel FocusedItem => gridView.GetRow(gridView.FocusedRowHandle) as FridgeSensorUIModel;

        private async void Timer_Tick(object sender, EventArgs e)
        {
            await ReloadAllSensorsStats();
        }

        private async Task ReloadAllSensorsStats()
        {
            if(Initialized == false)
            {
                return;
            }
            var items = grid.DataSource as BindingList<FridgeSensorUIModel>;
            foreach (var item in items)
            {
                if (item.Running == false)
                {
                    continue; //skip non-running sensor
                }
                var client = FridgeSensorClientFactory.GetClient(item.Port.Value);
                try
                {
                    var data = await client.GetSensorDataAsync();
                    var stats = new FridgeSensorStatsModel(data);
                    item.Stats.Add(stats);
                } catch
                {
                    Trace.WriteLine($"Error loading sensor {item.FridgeName} starts from port {item.Port}. The sensor had probably meanwhile been stopped.");
                }
            }
            grid.RefreshDataSource();
        }


        private ManagementClient Service => FridgeSensorsManagementClientFactory.Client;

        public async Task<List<FridgeSensorUIModel>> GetInitialEmptyItems()
        {
            var fridges = await Service.GetAllFridgesAsync();
            var names = fridges.Select(item => item.Name).ToList();
            var items = names.Select(name => new FridgeSensorUIModel() { FridgeName = name }).ToList();
            return items;
        }

        private async Task Initialize()
        {
            var items = await GetInitialEmptyItems();
            grid.DataSource = new BindingList<FridgeSensorUIModel>(items);
            Initialized = true;
        }

        /// <summary>
        /// Refreshes info about if that the sensor is running (for all sensors)
        /// </summary>
        /// <returns></returns>
        private async Task RefreshSensorsRunningInfo()
        {
            var items = grid.DataSource as BindingList<FridgeSensorUIModel>;
            foreach (var item in items)
            {
                int? maybePort = null;
                try
                {
                    maybePort = await Service.GetRunningSensorMicroservicePortAsync(item.FridgeName);
                }
                catch (ApiException ex)
                {
                    if (ex.StatusCode >= 400)
                    {
                        throw;
                    }
                }
                item.Port = maybePort;
                if (item.Port != null)
                {
                    item.Running = true;
                    //item.PeriodicUpdatingEnabled = true;
                }
            }
            grid.RefreshDataSource();
        }

        private async void reloadButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                await RefreshSensorsRunningInfo();
            } catch (ApiException ex)
            {
                if(ex.StatusCode >= 400)
                {
                    throw;
                }
            }
        }

        private async void initializeButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            await Initialize();
        }

        private async void startButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            var item = FocusedItem;
            if(item is null)
            {
                return;
            }
            if (item.Running)
            {
                return;
            }
            var name = item.FridgeName;
            var window = new SensorChoosePortWindow();
            var dialogResult = window.ShowDialog();
            if(dialogResult != DialogResult.OK)
            {
                return;
            }
            var port = window.Port;
            try
            {
                await Service.StartSensorMicroserviceAsync(name, port, true);
            } catch (Exception ex)
            {
                XtraMessageBox.Show($"{ex.GetType()} {ex.Message}", "The sensor microservice could not be started");
                return;
            }

            XtraMessageBox.Show( $"Sensor microservice {name} started on port {port}", "Success");
            item.Running = true;
            item.Port = port;
            grid.RefreshDataSource();
        }

        private async void stopButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            var item = FocusedItem;
            if (item is null)
            {
                return;
            }
            if (item.Running == false)
            {
                return;
            }

            var name = item.FridgeName;

            try
            {
                await Service.StopSensorMicroserviceAsync(name);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show($"{ex.GetType()} {ex.Message}", "The sensor microservice was not stopped");
                return;
            }

            XtraMessageBox.Show($"Sensor microservice {name} stopped", "Success");
            item.Running = false;
            item.Port = null;
            grid.RefreshDataSource();
        }

        private void gridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            var item = FocusedItem;
            if(item is null)
            {
                return;
            }

            if (item.Running)
            {
                startButton.Enabled = false;
                stopButton.Enabled = true;
            } else
            {
                startButton.Enabled = true;
                stopButton.Enabled = false;
            }
        }

        private void periodicUpdatingSwitch_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            PeriodicUpdatesEnabled = periodicUpdatingSwitch.Checked;
        }
    }
}
