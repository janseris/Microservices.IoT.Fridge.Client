﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;

using Microservices.IoT.Client.WinForms.APIClientFactories;
using Microservices.IoT.Client.WinForms.Models.Sensors.Traffic;
using Microservices.IoT.Sensors.Client.Traffic;

using Microsoft.AspNetCore.Http;

namespace Microservices.IoT.Client.WinForms.UserControls.Sensors.Traffic
{
    public partial class TrafficSensorsUC : DevExpress.XtraEditors.XtraUserControl
    {
        public TrafficSensorsUC()
        {
            InitializeComponent();
            (filterComboBox.Edit as RepositoryItemComboBox).Items.AddRange(PredefinedFilters);
        }

        private readonly List<string> PredefinedFilters = new List<string>()
        {
            "all",
            "sensorType:radar|speedLimit:60|location:Brno",
            "sensorType:radar",
        };

        private TrafficSensorsClient Service => TrafficSensorClientFactory.GetClient();

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Bindable(false)]
        [Browsable(false)]
        public string Filter
        {
            get => filterComboBox.EditValue as string;
            set => filterComboBox.EditValue = value;
        }

        private async void filterHelpButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var helpText = await Service.HelpSensorsAsync();
            XtraMessageBox.Show(helpText);
        }

        private async void reloadButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Filter))
            {
                XtraMessageBox.Show("Please, specify a filter");
                return;
            }

            ICollection<Traffic_Sensor> data = null;
            try
            {
                data = await Service.SensorsRequestAsync(Filter);
            }
            catch (Microservices.IoT.Sensors.Client.Traffic.ApiException ex)
            {
                if (ex.StatusCode == StatusCodes.Status400BadRequest)
                {
                    await ShowErrorMessageAndHelp();
                    return;
                }
            }
            var uselessCall = await Service.SensorsAcceptAsync(Filter);
            var items = data.Select(item => new TrafficSensorModel(item)).ToList();

            grid.DataSource = items;
        }

        private async Task ShowErrorMessageAndHelp()
        {
            var helpText = await Service.HelpSensorsAsync();
            string errorMessage = "The filter is not valid or no items found matching the filter.";
            XtraMessageBox.Show($"{errorMessage}{Environment.NewLine}{Environment.NewLine}{helpText}");
        }

        private void cancelFilterButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Filter = null;
        }
    }
}
