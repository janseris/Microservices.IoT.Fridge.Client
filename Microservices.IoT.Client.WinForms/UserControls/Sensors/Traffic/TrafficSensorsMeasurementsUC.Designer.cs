﻿namespace Microservices.IoT.Client.WinForms.UserControls.Sensors.Traffic
{
    partial class TrafficSensorsMeasurementsUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrafficSensorsMeasurementsUC));
            this.grid = new DevExpress.XtraGrid.GridControl();
            this.trafMeasModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicleSpeed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicleType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.reloadButton = new DevExpress.XtraBars.BarLargeButtonItem();
            this.filterTextEditLabel = new DevExpress.XtraBars.BarStaticItem();
            this.filterComboBox = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.filterHelpButton = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.cancelFilterButton = new DevExpress.XtraBars.BarLargeButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trafMeasModelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // grid
            // 
            this.grid.DataSource = this.trafMeasModelBindingSource;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.Location = new System.Drawing.Point(0, 55);
            this.grid.MainView = this.gridView;
            this.grid.MenuManager = this.barManager1;
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(1099, 589);
            this.grid.TabIndex = 11;
            this.grid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // trafMeasModelBindingSource
            // 
            this.trafMeasModelBindingSource.DataSource = typeof(Microservices.IoT.Client.WinForms.Models.Sensors.Traffic.TrafficSensorMeasurementModel);
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDate,
            this.colVehicleSpeed,
            this.colVehicleType});
            this.gridView.GridControl = this.grid;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsBehavior.ReadOnly = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowViewCaption = true;
            this.gridView.ViewCaption = "Traffic sensor measurements";
            // 
            // colDate
            // 
            this.colDate.FieldName = "Date";
            this.colDate.Name = "colDate";
            this.colDate.Visible = true;
            this.colDate.VisibleIndex = 0;
            this.colDate.Width = 196;
            // 
            // colVehicleSpeed
            // 
            this.colVehicleSpeed.AppearanceCell.Options.UseTextOptions = true;
            this.colVehicleSpeed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colVehicleSpeed.FieldName = "VehicleSpeed";
            this.colVehicleSpeed.Name = "colVehicleSpeed";
            this.colVehicleSpeed.Visible = true;
            this.colVehicleSpeed.VisibleIndex = 1;
            this.colVehicleSpeed.Width = 149;
            // 
            // colVehicleType
            // 
            this.colVehicleType.FieldName = "VehicleType";
            this.colVehicleType.Name = "colVehicleType";
            this.colVehicleType.Visible = true;
            this.colVehicleType.VisibleIndex = 2;
            this.colVehicleType.Width = 724;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.reloadButton,
            this.filterTextEditLabel,
            this.filterHelpButton,
            this.filterComboBox,
            this.cancelFilterButton});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 10;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemComboBox1});
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(398, 110);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.reloadButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.filterTextEditLabel, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.filterComboBox, "", false, true, true, 403),
            new DevExpress.XtraBars.LinkPersistInfo(this.cancelFilterButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.filterHelpButton)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // reloadButton
            // 
            this.reloadButton.Caption = "Reload";
            this.reloadButton.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.reloadButton.Id = 1;
            this.reloadButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("reloadButton.ImageOptions.Image")));
            this.reloadButton.Name = "reloadButton";
            this.reloadButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.reloadButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.reloadButton_ItemClick);
            // 
            // filterTextEditLabel
            // 
            this.filterTextEditLabel.Caption = "Filter";
            this.filterTextEditLabel.Id = 3;
            this.filterTextEditLabel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("filterTextEditLabel.ImageOptions.Image")));
            this.filterTextEditLabel.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("filterTextEditLabel.ImageOptions.LargeImage")));
            this.filterTextEditLabel.Name = "filterTextEditLabel";
            this.filterTextEditLabel.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // filterComboBox
            // 
            this.filterComboBox.Caption = "barEditItem1";
            this.filterComboBox.Edit = this.repositoryItemComboBox1;
            this.filterComboBox.Id = 7;
            this.filterComboBox.Name = "filterComboBox";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // filterHelpButton
            // 
            this.filterHelpButton.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Left;
            this.filterHelpButton.Description = "Show help for filters";
            this.filterHelpButton.Id = 6;
            this.filterHelpButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("filterHelpButton.ImageOptions.Image")));
            this.filterHelpButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("filterHelpButton.ImageOptions.LargeImage")));
            this.filterHelpButton.Name = "filterHelpButton";
            this.filterHelpButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.filterHelpButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.filterHelpButton_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1099, 55);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 644);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1099, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 55);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 589);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1099, 55);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 589);
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // cancelFilterButton
            // 
            this.cancelFilterButton.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Left;
            this.cancelFilterButton.Id = 9;
            this.cancelFilterButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barLargeButtonItem1.ImageOptions.Image")));
            this.cancelFilterButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barLargeButtonItem1.ImageOptions.LargeImage")));
            this.cancelFilterButton.Name = "cancelFilterButton";
            this.cancelFilterButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.cancelFilterButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cancelFilterButton_ItemClick);
            // 
            // TrafficSensorsMeasurementsUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grid);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "TrafficSensorsMeasurementsUC";
            this.Size = new System.Drawing.Size(1099, 644);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trafMeasModelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl grid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem reloadButton;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private System.Windows.Forms.BindingSource trafMeasModelBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colDate;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleSpeed;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleType;
        private DevExpress.XtraBars.BarStaticItem filterTextEditLabel;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarLargeButtonItem filterHelpButton;
        private DevExpress.XtraBars.BarEditItem filterComboBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraBars.BarLargeButtonItem cancelFilterButton;
    }
}
