﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using System.Windows.Forms;

using Microservices.IoT.Client.WinForms.APIClientFactories;
using Microservices.IoT.Client.WinForms.Models.Sensors.Temperature;
using Microservices.IoT.Client.WinForms.Windows.Sensors.Temperature;
using Microservices.IoT.Sensors.Client.Temperature;

namespace Microservices.IoT.Client.WinForms.UserControls
{
    public partial class TemperatureSensorsUC : DevExpress.XtraEditors.XtraUserControl
    {
        public TemperatureSensorsUC()
        {
            InitializeComponent();
        }
        private TemperatureSensorsClient Service => TemperatureSensorClientFactory.GetClient();

        private async void reloadButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            await Reload();
        }


        public async Task Reload()
        {
            var data = await Service.GetAllTempSensorAsync();
            var items = data.Select(item => new TemperatureSensorModel(item)).ToList();

            var manufacturers = await Service.GetAllManufacturersAsync();
            var types = await Service.GetAllTempSensorTypesAsync();

            FillRelatedEntityData(items, manufacturers, types);

            grid.DataSource = items;
        }

        private void FillRelatedEntityData(
            List<TemperatureSensorModel> sensors,
            ICollection<Manufacturer> manufacturers,
            ICollection<TempSensorType> sensorTypes)
        {
            foreach (var sensor in sensors)
            {
                var manufacturer = manufacturers.Where(m => m.ManufacturerId == sensor.TempSensorManufacturerId).SingleOrDefault();
                if(manufacturer != null)
                {
                    sensor.Manufacturer = manufacturer.ManufacturerName;
                }

                var type = sensorTypes.Where(m => m.TempSensorTypeId == sensor.TempSensorTypeId).SingleOrDefault();
                if (type != null)
                {
                    sensor.Type = type.SensorTypeName;
                }
            }
        }

        private async void addButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            AddTemperatureSensorWindow window = new AddTemperatureSensorWindow();
            var result = window.ShowDialog();
            if(result != DialogResult.OK)
            {
                return;
            }
            await Reload();
        }
    }
}
