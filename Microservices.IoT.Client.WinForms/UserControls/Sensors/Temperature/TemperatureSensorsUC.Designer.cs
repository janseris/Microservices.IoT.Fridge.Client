﻿namespace Microservices.IoT.Client.WinForms.UserControls
{
    partial class TemperatureSensorsUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TemperatureSensorsUC));
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.reloadButton = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.grid = new DevExpress.XtraGrid.GridControl();
            this.tempSensModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTempSensorId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTempValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManufacturer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberOpenPorts = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreationDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastTimeChecked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.addButton = new DevExpress.XtraBars.BarLargeButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempSensModelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            this.SuspendLayout();
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1038, 55);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 504);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.reloadButton,
            this.addButton});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 5;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(398, 110);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.reloadButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.addButton)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // reloadButton
            // 
            this.reloadButton.Caption = "Reload";
            this.reloadButton.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.reloadButton.Id = 1;
            this.reloadButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("reloadButton.ImageOptions.Image")));
            this.reloadButton.Name = "reloadButton";
            this.reloadButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.reloadButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.reloadButton_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1038, 55);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 559);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1038, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 55);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 504);
            // 
            // grid
            // 
            this.grid.DataSource = this.tempSensModelBindingSource;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.Location = new System.Drawing.Point(0, 55);
            this.grid.MainView = this.gridView;
            this.grid.MenuManager = this.barManager1;
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(1038, 504);
            this.grid.TabIndex = 9;
            this.grid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // tempSensModelBindingSource
            // 
            this.tempSensModelBindingSource.DataSource = typeof(Microservices.IoT.Client.WinForms.Models.Sensors.Temperature.TemperatureSensorModel);
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTempSensorId,
            this.colName,
            this.colTempValue,
            this.colType,
            this.colManufacturer,
            this.colNumberOpenPorts,
            this.colCreationDateTime,
            this.colLastTimeChecked});
            this.gridView.GridControl = this.grid;
            this.gridView.Name = "gridView";
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowViewCaption = true;
            this.gridView.ViewCaption = "Temperature sensors";
            // 
            // colTempSensorId
            // 
            this.colTempSensorId.FieldName = "TempSensorId";
            this.colTempSensorId.Name = "colTempSensorId";
            this.colTempSensorId.Visible = true;
            this.colTempSensorId.VisibleIndex = 0;
            this.colTempSensorId.Width = 40;
            // 
            // colName
            // 
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 2;
            this.colName.Width = 137;
            // 
            // colTempValue
            // 
            this.colTempValue.FieldName = "TempValue";
            this.colTempValue.Name = "colTempValue";
            this.colTempValue.Visible = true;
            this.colTempValue.VisibleIndex = 6;
            this.colTempValue.Width = 137;
            // 
            // colType
            // 
            this.colType.FieldName = "Type";
            this.colType.Name = "colType";
            this.colType.Visible = true;
            this.colType.VisibleIndex = 7;
            this.colType.Width = 151;
            // 
            // colManufacturer
            // 
            this.colManufacturer.FieldName = "Manufacturer";
            this.colManufacturer.Name = "colManufacturer";
            this.colManufacturer.Visible = true;
            this.colManufacturer.VisibleIndex = 1;
            this.colManufacturer.Width = 137;
            // 
            // colNumberOpenPorts
            // 
            this.colNumberOpenPorts.FieldName = "NumberOpenPorts";
            this.colNumberOpenPorts.Name = "colNumberOpenPorts";
            this.colNumberOpenPorts.Visible = true;
            this.colNumberOpenPorts.VisibleIndex = 3;
            this.colNumberOpenPorts.Width = 137;
            // 
            // colCreationDateTime
            // 
            this.colCreationDateTime.FieldName = "CreationDateTime";
            this.colCreationDateTime.Name = "colCreationDateTime";
            this.colCreationDateTime.Visible = true;
            this.colCreationDateTime.VisibleIndex = 4;
            this.colCreationDateTime.Width = 137;
            // 
            // colLastTimeChecked
            // 
            this.colLastTimeChecked.FieldName = "LastTimeChecked";
            this.colLastTimeChecked.Name = "colLastTimeChecked";
            this.colLastTimeChecked.Visible = true;
            this.colLastTimeChecked.VisibleIndex = 5;
            this.colLastTimeChecked.Width = 137;
            // 
            // addButton
            // 
            this.addButton.Caption = "Add";
            this.addButton.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.addButton.Id = 4;
            this.addButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barLargeButtonItem1.ImageOptions.Image")));
            this.addButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barLargeButtonItem1.ImageOptions.LargeImage")));
            this.addButton.Name = "addButton";
            this.addButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.addButton_ItemClick);
            // 
            // TemperatureSensorsUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grid);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "TemperatureSensorsUC";
            this.Size = new System.Drawing.Size(1038, 559);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempSensModelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem reloadButton;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraGrid.GridControl grid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private System.Windows.Forms.BindingSource tempSensModelBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTempSensorId;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberOpenPorts;
        private DevExpress.XtraGrid.Columns.GridColumn colCreationDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colLastTimeChecked;
        private DevExpress.XtraGrid.Columns.GridColumn colTempValue;
        private DevExpress.XtraGrid.Columns.GridColumn colType;
        private DevExpress.XtraGrid.Columns.GridColumn colManufacturer;
        private DevExpress.XtraBars.BarLargeButtonItem addButton;
    }
}
