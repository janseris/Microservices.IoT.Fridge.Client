﻿namespace Microservices.IoT.Client.WinForms.UserControls
{
    partial class FridgesAndEventsUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.fridgesUC = new Microservices.IoT.Client.WinForms.UserControls.FridgesUC();
            this.eventsUC = new Microservices.IoT.Client.WinForms.UserControls.EventsUC();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.fridgesUC, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.eventsUC, 1, 0);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 1;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(961, 476);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // fridgesUC
            // 
            this.fridgesUC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fridgesUC.Location = new System.Drawing.Point(3, 3);
            this.fridgesUC.Name = "fridgesUC";
            this.fridgesUC.Size = new System.Drawing.Size(474, 470);
            this.fridgesUC.TabIndex = 0;
            this.fridgesUC.FocusedChanged += new System.EventHandler<Microservices.IoT.Client.WinForms.Models.Fridges.FridgeModel>(this.fridgesUC_FocusedChanged);
            // 
            // eventsUC
            // 
            this.eventsUC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventsUC.ID = null;
            this.eventsUC.Location = new System.Drawing.Point(483, 3);
            this.eventsUC.Name = "eventsUC";
            this.eventsUC.Size = new System.Drawing.Size(475, 470);
            this.eventsUC.TabIndex = 1;
            // 
            // FridgesAndEventsUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "FridgesAndEventsUC";
            this.Size = new System.Drawing.Size(961, 476);
            this.tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private FridgesUC fridgesUC;
        private EventsUC eventsUC;
    }
}
