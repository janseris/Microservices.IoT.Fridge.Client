﻿using System.Windows.Forms;

namespace Microservices.IoT.Client.WinForms.UserControls
{
    public partial class FridgesAndEventsUC : DevExpress.XtraEditors.XtraUserControl
    {
        public FridgesAndEventsUC()
        {
            InitializeComponent();
        }

        private async void fridgesUC_FocusedChanged(object sender, Models.Fridges.FridgeModel e)
        {
            eventsUC.ID = e.ID;
            await eventsUC.Reload();
        }
    }
}
