﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using Microservices.IoT.Client.WinForms.APIClientFactories;
using Microservices.IoT.Client.WinForms.Models.Fridges;

namespace Microservices.IoT.Client.WinForms.UserControls
{
    public partial class FridgesUC : DevExpress.XtraEditors.XtraUserControl
    {
        public FridgesUC()
        {
            InitializeComponent();
        }

        private ManagementClient Service => FridgeSensorsManagementClientFactory.Client;

        public event EventHandler<FridgeModel> FocusedChanged;

        private FridgeModel FocusedItem => gridView.GetRow(gridView.FocusedRowHandle) as FridgeModel;

        public async Task Reload()
        {
            var data = await Service.GetAllFridgesAsync();
            var items = data.Select(item => new FridgeModel(item)).ToList();
            grid.DataSource = items;
        }

        private void gridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            var item = FocusedItem;
            if(item is null)
            {
                return;
            }
            
            FocusedChanged?.Invoke(this, item);
            temperatureDegreesText.Caption = $"{item.Info.Sensors.TemperatureDegrees} °C";
            powerConsumptionText.Caption = $"{item.Info.Sensors.PowerConsumptionWatts} W";
        }

        private async void reloadButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            await Reload();
        }

        private async void increaseTemperatureButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var item = FocusedItem;
            if(item is null)
            {
                return;
            }
            var temperature = item.Info.Sensors.TemperatureDegrees;
            await Service.SetFridgeCurrentTemperatureAsync(item.ID, temperature + 1);
            await Reload();
        }

        private async void decreaseTemperatureButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var item = FocusedItem;
            if (item is null)
            {
                return;
            }
            var temperature = item.Info.Sensors.TemperatureDegrees;
            await Service.SetFridgeCurrentTemperatureAsync(item.ID, temperature + -1);
            await Reload();
        }

        private async void increasePowerConsumptionButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var item = FocusedItem;
            if (item is null)
            {
                return;
            }
            var powerConsumptionWatts = item.Info.Sensors.PowerConsumptionWatts;
            await Service.SetFridgeCurrentPowerConsumptionAsync(item.ID, powerConsumptionWatts + 1);
            await Reload();
        }

        private async void decreasePowerConsumptionButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var item = FocusedItem;
            if (item is null)
            {
                return;
            }
            var powerConsumptionWatts = item.Info.Sensors.PowerConsumptionWatts;
            await Service.SetFridgeCurrentPowerConsumptionAsync(item.ID, powerConsumptionWatts - 1);
            await Reload();
        }
    }
}
