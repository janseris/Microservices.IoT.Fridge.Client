﻿namespace Microservices.IoT.Client.WinForms.UserControls
{
    partial class FridgesUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FridgesUC));
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.reloadButton = new DevExpress.XtraBars.BarLargeButtonItem();
            this.temperatureDegreesLabel = new DevExpress.XtraBars.BarStaticItem();
            this.temperatureDegreesText = new DevExpress.XtraBars.BarStaticItem();
            this.increaseTemperatureButton = new DevExpress.XtraBars.BarButtonItem();
            this.decreaseTemperatureButton = new DevExpress.XtraBars.BarButtonItem();
            this.powerConsumptionLabel = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.powerConsumptionText = new DevExpress.XtraBars.BarStaticItem();
            this.increasePowerConsumptionButton = new DevExpress.XtraBars.BarButtonItem();
            this.decreasePowerConsumptionButton = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemPageNumberEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPageNumberEdit();
            this.fridgeModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grid = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBrand = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInfo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOccupiedVolumeLiters = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOccupiedVolumePercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFreeVolumePercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFreeVolumeLiters = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPageNumberEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fridgeModelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            this.SuspendLayout();
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(909, 55);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 417);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.reloadButton,
            this.temperatureDegreesLabel,
            this.increaseTemperatureButton,
            this.decreaseTemperatureButton,
            this.powerConsumptionLabel,
            this.barStaticItem3,
            this.temperatureDegreesText,
            this.powerConsumptionText,
            this.increasePowerConsumptionButton,
            this.barButtonItem4,
            this.decreasePowerConsumptionButton});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 13;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPageNumberEdit1});
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.reloadButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.temperatureDegreesLabel, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.temperatureDegreesText),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.increaseTemperatureButton, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.decreaseTemperatureButton, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.powerConsumptionLabel, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.powerConsumptionText),
            new DevExpress.XtraBars.LinkPersistInfo(this.increasePowerConsumptionButton),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.decreasePowerConsumptionButton, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // reloadButton
            // 
            this.reloadButton.Caption = "Reload";
            this.reloadButton.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.reloadButton.Id = 1;
            this.reloadButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("reloadButton.ImageOptions.Image")));
            this.reloadButton.Name = "reloadButton";
            this.reloadButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.reloadButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.reloadButton_ItemClick);
            // 
            // temperatureDegreesLabel
            // 
            this.temperatureDegreesLabel.Caption = "Temperature";
            this.temperatureDegreesLabel.Id = 2;
            this.temperatureDegreesLabel.Name = "temperatureDegreesLabel";
            // 
            // temperatureDegreesText
            // 
            this.temperatureDegreesText.Caption = "°C";
            this.temperatureDegreesText.Id = 8;
            this.temperatureDegreesText.Name = "temperatureDegreesText";
            // 
            // increaseTemperatureButton
            // 
            this.increaseTemperatureButton.Id = 4;
            this.increaseTemperatureButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("increaseTemperatureButton.ImageOptions.Image")));
            this.increaseTemperatureButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("increaseTemperatureButton.ImageOptions.LargeImage")));
            this.increaseTemperatureButton.Name = "increaseTemperatureButton";
            this.increaseTemperatureButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.increaseTemperatureButton_ItemClick);
            // 
            // decreaseTemperatureButton
            // 
            this.decreaseTemperatureButton.Id = 5;
            this.decreaseTemperatureButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("decreaseTemperatureButton.ImageOptions.Image")));
            this.decreaseTemperatureButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("decreaseTemperatureButton.ImageOptions.LargeImage")));
            this.decreaseTemperatureButton.Name = "decreaseTemperatureButton";
            this.decreaseTemperatureButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.decreaseTemperatureButton_ItemClick);
            // 
            // powerConsumptionLabel
            // 
            this.powerConsumptionLabel.Caption = "Power consumption";
            this.powerConsumptionLabel.Id = 6;
            this.powerConsumptionLabel.Name = "powerConsumptionLabel";
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Id = 7;
            this.barStaticItem3.Name = "barStaticItem3";
            // 
            // powerConsumptionText
            // 
            this.powerConsumptionText.Caption = "W";
            this.powerConsumptionText.Id = 9;
            this.powerConsumptionText.Name = "powerConsumptionText";
            // 
            // increasePowerConsumptionButton
            // 
            this.increasePowerConsumptionButton.Caption = "barButtonItem3";
            this.increasePowerConsumptionButton.Id = 10;
            this.increasePowerConsumptionButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("increasePowerConsumptionButton.ImageOptions.Image")));
            this.increasePowerConsumptionButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("increasePowerConsumptionButton.ImageOptions.LargeImage")));
            this.increasePowerConsumptionButton.Name = "increasePowerConsumptionButton";
            this.increasePowerConsumptionButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.increasePowerConsumptionButton_ItemClick);
            // 
            // decreasePowerConsumptionButton
            // 
            this.decreasePowerConsumptionButton.Id = 12;
            this.decreasePowerConsumptionButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("decreasePowerConsumptionButton.ImageOptions.Image")));
            this.decreasePowerConsumptionButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("decreasePowerConsumptionButton.ImageOptions.LargeImage")));
            this.decreasePowerConsumptionButton.Name = "decreasePowerConsumptionButton";
            this.decreasePowerConsumptionButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.decreasePowerConsumptionButton_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(909, 55);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 472);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(909, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 55);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 417);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "barButtonItem4";
            this.barButtonItem4.Id = 11;
            this.barButtonItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.Image")));
            this.barButtonItem4.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.LargeImage")));
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // repositoryItemPageNumberEdit1
            // 
            this.repositoryItemPageNumberEdit1.AutoHeight = false;
            this.repositoryItemPageNumberEdit1.Mask.EditMask = "########;";
            this.repositoryItemPageNumberEdit1.Name = "repositoryItemPageNumberEdit1";
            // 
            // fridgeModelBindingSource
            // 
            this.fridgeModelBindingSource.DataSource = typeof(Microservices.IoT.Client.WinForms.Models.Fridges.FridgeModel);
            // 
            // grid
            // 
            this.grid.DataSource = this.fridgeModelBindingSource;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.Location = new System.Drawing.Point(0, 55);
            this.grid.MainView = this.gridView;
            this.grid.MenuManager = this.barManager1;
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(909, 417);
            this.grid.TabIndex = 9;
            this.grid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colName,
            this.colBrand,
            this.colType,
            this.colInfo,
            this.colOccupiedVolumeLiters,
            this.colOccupiedVolumePercent,
            this.colFreeVolumePercent,
            this.colFreeVolumeLiters});
            this.gridView.GridControl = this.grid;
            this.gridView.Name = "gridView";
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowViewCaption = true;
            this.gridView.ViewCaption = "Fridges";
            this.gridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView_FocusedRowChanged);
            // 
            // colID
            // 
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.Visible = true;
            this.colID.VisibleIndex = 0;
            // 
            // colName
            // 
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 1;
            // 
            // colBrand
            // 
            this.colBrand.FieldName = "Brand";
            this.colBrand.Name = "colBrand";
            this.colBrand.Visible = true;
            this.colBrand.VisibleIndex = 2;
            // 
            // colType
            // 
            this.colType.FieldName = "Type";
            this.colType.Name = "colType";
            this.colType.Visible = true;
            this.colType.VisibleIndex = 3;
            // 
            // colInfo
            // 
            this.colInfo.FieldName = "Info";
            this.colInfo.Name = "colInfo";
            this.colInfo.Visible = true;
            this.colInfo.VisibleIndex = 4;
            // 
            // colOccupiedVolumeLiters
            // 
            this.colOccupiedVolumeLiters.FieldName = "OccupiedVolumeLiters";
            this.colOccupiedVolumeLiters.Name = "colOccupiedVolumeLiters";
            this.colOccupiedVolumeLiters.OptionsColumn.ReadOnly = true;
            this.colOccupiedVolumeLiters.Visible = true;
            this.colOccupiedVolumeLiters.VisibleIndex = 5;
            // 
            // colOccupiedVolumePercent
            // 
            this.colOccupiedVolumePercent.FieldName = "OccupiedVolumePercent";
            this.colOccupiedVolumePercent.Name = "colOccupiedVolumePercent";
            this.colOccupiedVolumePercent.OptionsColumn.ReadOnly = true;
            this.colOccupiedVolumePercent.Visible = true;
            this.colOccupiedVolumePercent.VisibleIndex = 6;
            // 
            // colFreeVolumePercent
            // 
            this.colFreeVolumePercent.FieldName = "FreeVolumePercent";
            this.colFreeVolumePercent.Name = "colFreeVolumePercent";
            this.colFreeVolumePercent.OptionsColumn.ReadOnly = true;
            this.colFreeVolumePercent.Visible = true;
            this.colFreeVolumePercent.VisibleIndex = 7;
            // 
            // colFreeVolumeLiters
            // 
            this.colFreeVolumeLiters.FieldName = "FreeVolumeLiters";
            this.colFreeVolumeLiters.Name = "colFreeVolumeLiters";
            this.colFreeVolumeLiters.OptionsColumn.ReadOnly = true;
            this.colFreeVolumeLiters.Visible = true;
            this.colFreeVolumeLiters.VisibleIndex = 8;
            // 
            // FridgesUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grid);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "FridgesUC";
            this.Size = new System.Drawing.Size(909, 472);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPageNumberEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fridgeModelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem reloadButton;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private System.Windows.Forms.BindingSource fridgeModelBindingSource;
        private DevExpress.XtraGrid.GridControl grid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colBrand;
        private DevExpress.XtraGrid.Columns.GridColumn colType;
        private DevExpress.XtraGrid.Columns.GridColumn colInfo;
        private DevExpress.XtraGrid.Columns.GridColumn colOccupiedVolumeLiters;
        private DevExpress.XtraGrid.Columns.GridColumn colOccupiedVolumePercent;
        private DevExpress.XtraGrid.Columns.GridColumn colFreeVolumePercent;
        private DevExpress.XtraGrid.Columns.GridColumn colFreeVolumeLiters;
        private DevExpress.XtraBars.BarStaticItem temperatureDegreesLabel;
        private DevExpress.XtraBars.BarStaticItem temperatureDegreesText;
        private DevExpress.XtraBars.BarButtonItem increaseTemperatureButton;
        private DevExpress.XtraBars.BarButtonItem decreaseTemperatureButton;
        private DevExpress.XtraBars.BarStaticItem powerConsumptionLabel;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem powerConsumptionText;
        private DevExpress.XtraBars.BarButtonItem increasePowerConsumptionButton;
        private DevExpress.XtraBars.BarButtonItem decreasePowerConsumptionButton;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraEditors.Repository.RepositoryItemPageNumberEdit repositoryItemPageNumberEdit1;
    }
}
