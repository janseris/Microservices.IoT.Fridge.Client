﻿using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using Microservices.IoT.Client.WinForms.APIClientFactories;
using Microservices.IoT.Client.WinForms.Models.Events;

namespace Microservices.IoT.Client.WinForms.UserControls
{
    public partial class EventTypesUC : DevExpress.XtraEditors.XtraUserControl
    {
        public EventTypesUC()
        {
            InitializeComponent();
        }

        private ManagementClient Service => FridgeSensorsManagementClientFactory.Client;

        public async Task Reload()
        {
            var data = await Service.GetEventTypesAsync();
            var items = data.Select(item => new EventTypeModel(item)).ToList();
            grid.DataSource = items;
        }

        private async void reloadButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            await Reload();
        }
    }
}
