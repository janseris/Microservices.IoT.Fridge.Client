﻿using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using Microservices.IoT.Client.WinForms.APIClientFactories;
using Microservices.IoT.Client.WinForms.Models.Events;

namespace Microservices.IoT.Client.WinForms.UserControls
{
    /// <summary>
    /// Used to show events for a fridge by <see cref="ID"/>
    /// </summary>
    public partial class EventsUC : DevExpress.XtraEditors.XtraUserControl
    {
        public EventsUC()
        {
            InitializeComponent();
        }

        private ManagementClient Service => FridgeSensorsManagementClientFactory.Client;

        public int? ID { get; set; }

        public async Task Reload()
        {
            if(ID is null)
            {
                return;
            }
            var data = await Service.GetAllEventsAsync(ID);
            var items = data.Select(item => new EventModel(item)).ToList();
            grid.DataSource = items;
        }

        private async void reloadButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            await Reload();
        }
    }
}
