﻿using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using Microservices.IoT.Client.WinForms.APIClientFactories;
using Microservices.IoT.Client.WinForms.Models.FoodItems;
using Microservices.IoT.Client.WinForms.Windows;

namespace Microservices.IoT.Client.WinForms.UserControls
{
    public partial class FoodUC : DevExpress.XtraEditors.XtraUserControl
    {
        public FoodUC()
        {
            InitializeComponent();
        }

        private ManagementClient Service => FridgeSensorsManagementClientFactory.Client;

        public async Task Reload()
        {
            var data = await Service.GetAllFoodItemsAsync();
            var items = data.Select(item => new FoodModel(item)).ToList();
            grid.DataSource = items;
        }

        private async void reloadButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            await Reload();
        }

        private void addButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var window = new AddFoodWindow();
            window.Show();
        }
    }
}
