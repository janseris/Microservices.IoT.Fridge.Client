﻿using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using Microservices.IoT.Client.WinForms.APIClientFactories;
using Microservices.IoT.Client.WinForms.Models.FoodItems;

namespace Microservices.IoT.Client.WinForms.UserControls
{
    public partial class FoodTypesUC : DevExpress.XtraEditors.XtraUserControl
    {
        public FoodTypesUC()
        {
            InitializeComponent();
        }
        private ManagementClient Service => FridgeSensorsManagementClientFactory.Client;

        public async Task Reload()
        {
            var data = await Service.GetAllFoodTypesAsync();
            var items = data.Select(item => new FoodTypeModel(item)).ToList();
            grid.DataSource = items;
        }

        private async void reloadButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            await Reload();
        }
    }
}
