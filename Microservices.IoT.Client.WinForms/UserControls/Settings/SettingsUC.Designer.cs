﻿namespace Microservices.IoT.Client.WinForms.UserControls.Settings
{
    partial class SettingsUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.managementConsoleBaseURLLabel = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.temperatureSensorsAPIURLTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.managementConsoleURLTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.trafficSensorsAPIURLTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureSensorsAPIURLTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.managementConsoleURLTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trafficSensorsAPIURLTextEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.managementConsoleBaseURLLabel, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelControl1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.temperatureSensorsAPIURLTextEdit, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.labelControl2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.managementConsoleURLTextEdit, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.trafficSensorsAPIURLTextEdit, 1, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(828, 587);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // managementConsoleBaseURLLabel
            // 
            this.managementConsoleBaseURLLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.managementConsoleBaseURLLabel.Appearance.Options.UseFont = true;
            this.managementConsoleBaseURLLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.managementConsoleBaseURLLabel.Location = new System.Drawing.Point(4, 4);
            this.managementConsoleBaseURLLabel.Margin = new System.Windows.Forms.Padding(4);
            this.managementConsoleBaseURLLabel.Name = "managementConsoleBaseURLLabel";
            this.managementConsoleBaseURLLabel.Size = new System.Drawing.Size(180, 34);
            this.managementConsoleBaseURLLabel.TabIndex = 6;
            this.managementConsoleBaseURLLabel.Text = "Management Console Base URL";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl1.Location = new System.Drawing.Point(3, 45);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(182, 34);
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "Temperature sensors API URL";
            // 
            // temperatureSensorsAPIURLTextEdit
            // 
            this.temperatureSensorsAPIURLTextEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.temperatureSensorsAPIURLTextEdit.Location = new System.Drawing.Point(191, 45);
            this.temperatureSensorsAPIURLTextEdit.Name = "temperatureSensorsAPIURLTextEdit";
            this.temperatureSensorsAPIURLTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.temperatureSensorsAPIURLTextEdit.Properties.Appearance.Options.UseFont = true;
            this.temperatureSensorsAPIURLTextEdit.Size = new System.Drawing.Size(778, 34);
            this.temperatureSensorsAPIURLTextEdit.TabIndex = 14;
            this.temperatureSensorsAPIURLTextEdit.EditValueChanged += new System.EventHandler(this.temperatureSensorsAPIURLTextEdit_EditValueChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl2.Location = new System.Drawing.Point(3, 85);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(182, 34);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "Traffic sensors API URL";
            // 
            // managementConsoleURLTextEdit
            // 
            this.managementConsoleURLTextEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.managementConsoleURLTextEdit.Location = new System.Drawing.Point(192, 4);
            this.managementConsoleURLTextEdit.Margin = new System.Windows.Forms.Padding(4);
            this.managementConsoleURLTextEdit.Name = "managementConsoleURLTextEdit";
            this.managementConsoleURLTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.managementConsoleURLTextEdit.Properties.Appearance.Options.UseFont = true;
            this.managementConsoleURLTextEdit.Size = new System.Drawing.Size(776, 34);
            this.managementConsoleURLTextEdit.TabIndex = 4;
            this.managementConsoleURLTextEdit.EditValueChanged += new System.EventHandler(this.managementConsoleURLTextEdit_EditValueChanged);
            // 
            // trafficSensorsAPIURLTextEdit
            // 
            this.trafficSensorsAPIURLTextEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trafficSensorsAPIURLTextEdit.Location = new System.Drawing.Point(191, 85);
            this.trafficSensorsAPIURLTextEdit.Name = "trafficSensorsAPIURLTextEdit";
            this.trafficSensorsAPIURLTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.trafficSensorsAPIURLTextEdit.Properties.Appearance.Options.UseFont = true;
            this.trafficSensorsAPIURLTextEdit.Size = new System.Drawing.Size(778, 34);
            this.trafficSensorsAPIURLTextEdit.TabIndex = 15;
            this.trafficSensorsAPIURLTextEdit.EditValueChanged += new System.EventHandler(this.trafficSensorsAPIURLTextEdit_EditValueChanged);
            // 
            // SettingsUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "SettingsUC";
            this.Size = new System.Drawing.Size(828, 587);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureSensorsAPIURLTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.managementConsoleURLTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trafficSensorsAPIURLTextEdit.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevExpress.XtraEditors.TextEdit managementConsoleURLTextEdit;
        private DevExpress.XtraEditors.LabelControl managementConsoleBaseURLLabel;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit temperatureSensorsAPIURLTextEdit;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit trafficSensorsAPIURLTextEdit;
    }
}
