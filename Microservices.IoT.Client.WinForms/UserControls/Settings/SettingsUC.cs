﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Microservices.IoT.Client.WinForms.UserControls.Settings
{
    public partial class SettingsUC : DevExpress.XtraEditors.XtraUserControl
    {
        public SettingsUC()
        {
            InitializeComponent();
            managementConsoleURLTextEdit.Text = AppState.ManagementConsoleBaseURL;
            temperatureSensorsAPIURLTextEdit.Text = AppState.TemperatureSensorsAPIBaseURL;
            trafficSensorsAPIURLTextEdit.Text = AppState.TrafficSensorsAPIBaseURL;
        }

        #region fridge sensors management console URL

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Bindable(false)]
        [Browsable(false)]
        public string ManagementConsoleBaseURL
        {
            get => AppState.ManagementConsoleBaseURL;
            set => AppState.ManagementConsoleBaseURL = value;
        }

        private void managementConsoleURLTextEdit_EditValueChanged(object sender, EventArgs e)
        {
            ManagementConsoleBaseURL = managementConsoleURLTextEdit.Text;
        }

        #endregion

        #region temperature sensors API URL

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Bindable(false)]
        [Browsable(false)]
        public string TemperatureSensorsAPIBaseURL
        {
            get => AppState.TemperatureSensorsAPIBaseURL;
            set => AppState.TemperatureSensorsAPIBaseURL = value;
        }

        private void temperatureSensorsAPIURLTextEdit_EditValueChanged(object sender, EventArgs e)
        {
            TemperatureSensorsAPIBaseURL = temperatureSensorsAPIURLTextEdit.Text;
        }

        #endregion

        #region traffic sensors API URL

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Bindable(false)]
        [Browsable(false)]
        public string TrafficSensorsAPIBaseURL
        {
            get => AppState.TrafficSensorsAPIBaseURL;
            set => AppState.TrafficSensorsAPIBaseURL = value;
        }

        private void trafficSensorsAPIURLTextEdit_EditValueChanged(object sender, EventArgs e)
        {
            TrafficSensorsAPIBaseURL = trafficSensorsAPIURLTextEdit.Text;
        }

        #endregion
    }
}
