﻿namespace Microservices.IoT.Client.WinForms
{
    public class AppState
    {
        public static string ManagementConsoleBaseURL { get; set; } = "https://localhost:7258/";
        public static string TemperatureSensorsAPIBaseURL { get; set; } = "https://e19f-2001-718-801-22c-6177-9fbe-ca22-4c21.eu.ngrok.io/";
        public static string TrafficSensorsAPIBaseURL { get; set; } = "https://90c2-147-251-208-241.eu.ngrok.io/";
    }
}
