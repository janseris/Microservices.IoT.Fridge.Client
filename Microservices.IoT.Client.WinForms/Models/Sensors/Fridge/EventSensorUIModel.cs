﻿using System;

using Microservices.IoT.Sensors.Client;

namespace Microservices.IoT.Client.WinForms.Models.Sensors
{
    public class EventSensorUIModel
    {
        public string Type { get; set; }

        /// <summary>
        /// Automatically filled when the event starts
        /// </summary>
        public DateTime From { get; set; }

        /// <summary>
        /// Filled when the event ends
        /// </summary>
        public DateTime? To { get; set; }

        public EventSensorUIModel(EventSensorModel data)
        {
            Type = data.Type;
            From = data.From.DateTime;
            To = data.To?.DateTime;
        }
    }
}
