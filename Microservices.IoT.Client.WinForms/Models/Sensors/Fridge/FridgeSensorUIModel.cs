﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Microservices.IoT.Client.WinForms.Models.Sensors
{
    public class FridgeSensorUIModel
    {
        public string FridgeName { get; set; }
        public int? Port { get; set; } = null;

        [Display(Name = "Last update")]
        [DisplayFormat(DataFormatString = "HH:MM:ss.fff")]
        public DateTime? LastUpdate { get; set; }

        [Display(Name = "Update period seconds")]
        public int UpdatePeriodSeconds { get; set; } = 1;

        [Display(Name = "Periodic updating on")]
        public bool PeriodicUpdatingEnabled { get; set; } = false;

        /// <summary>
        /// If the sensor microservice instance is running on the server
        /// </summary>
        public bool Running { get; set; } = false;

        public BindingList<FridgeSensorStatsModel> Stats { get; set; } = new BindingList<FridgeSensorStatsModel>();
    }
}
