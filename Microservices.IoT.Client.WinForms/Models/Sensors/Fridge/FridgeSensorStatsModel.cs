﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

using Microservices.IoT.Sensors.Client;

namespace Microservices.IoT.Client.WinForms.Models.Sensors
{
    public class FridgeSensorStatsModel
    {
        /// <summary>
        /// Degrees celsius
        /// </summary>
        [Display(Name = "Temperature [°C]")]
        public double Temperature { get; set; }

        /// <summary>
        /// Watts
        /// </summary>
        [Display(Name = "Power consumption [W]")]
        public int PowerConsumption { get; set; }

        [Display(Name = "Door open")]
        public bool IsDoorOpen { get; set; }

        [Display(Name = "Warnings")]
        public BindingList<string> CurrentWarnings { get; set; }

        public FridgeSensorStatsModel(FridgeSensorStats data)
        {
            Temperature = data.Temperature;
            PowerConsumption = data.PowerConsumption;
            IsDoorOpen = data.IsDoorOpen;
            CurrentWarnings = new BindingList<string>(data.CurrentWarnings.ToList()); //copy
        }
    }
}
