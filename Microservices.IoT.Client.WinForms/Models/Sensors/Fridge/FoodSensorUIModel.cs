﻿using System;
using System.ComponentModel.DataAnnotations;

using Microservices.IoT.Sensors.Client;

namespace Microservices.IoT.Client.WinForms.Models.Sensors
{
    public class FoodSensorUIModel
    {
        public string Name { get; set; }
        public string Type { get; set; }

        /// <summary>
        /// If the food item's packaging had been open
        /// </summary>
        public bool Open { get; set; }

        [Display(Name = "Initial weight grams")]
        public int InitialWeightGrams { get; set; }

        /// <summary>
        /// Is reduced by consuming the food
        /// </summary>
        [Display(Name = "Current weight grams")]
        public int CurrentWeightGrams { get; set; }

        [Display(Name = "Expiration date")]
        public DateTime ExpirationDate { get; set; }

        public FoodSensorUIModel(FoodSensorModel data)
        {
            Name = data.Name;
            Type = data.Type;
            Open = data.Open;
            InitialWeightGrams = data.InitialWeightGrams;
            CurrentWeightGrams = data.CurrentWeightGrams;
            ExpirationDate = data.ExpirationDate.DateTime;
        }
    }
}
