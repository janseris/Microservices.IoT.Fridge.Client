﻿using System.ComponentModel.DataAnnotations;

namespace Microservices.IoT.Client.WinForms.Models.Sensors.Traffic
{
    public enum VehicleTypeModel
    {
        [Display(Name = "Motocycle")]
        Motocycle,
        [Display(Name = "Car")]
        Car,
        [Display(Name = "Van")]
        Van,
        [Display(Name = "Truck")]
        Truck,
        [Display(Name = "Bus")]
        Bus,
        [Display(Name = "Unknown")]
        Unknown
    }
}
