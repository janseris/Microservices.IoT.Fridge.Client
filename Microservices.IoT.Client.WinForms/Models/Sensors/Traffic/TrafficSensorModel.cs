﻿using System.ComponentModel.DataAnnotations;

using Microservices.IoT.Sensors.Client.Traffic;

namespace Microservices.IoT.Client.WinForms.Models.Sensors.Traffic
{
    public class TrafficSensorModel
    {
        [Display(Name = "Location")]
        public string Location { get; set; }
        
        [Display(Name = "Type")]
        public TrafficSensorTypeModel Type { get; set; }
        
        [Display(Name = "Speed limit")]
        public int SpeedLimit { get; set; }

        public TrafficSensorModel(Traffic_Sensor data)
        {
            Location = data._location;
            Type = (TrafficSensorTypeModel)(int)data._sensorType;
            SpeedLimit = data._speedLimit;
        }
    }
}
