﻿using System.ComponentModel.DataAnnotations;

namespace Microservices.IoT.Client.WinForms.Models.Sensors.Traffic
{
    public enum TrafficSensorTypeModel
    {
        [Display(Name = "Camera")]
        Camera,
        [Display(Name = "Radar")]
        Radar,
        [Display(Name = "Grid")]
        Grid
    }
}
