﻿using System;
using System.ComponentModel.DataAnnotations;

using Microservices.IoT.Sensors.Client.Traffic;

namespace Microservices.IoT.Client.WinForms.Models.Sensors.Traffic
{
    public class TrafficSensorMeasurementModel
    {
        [Display(Name = "Date of measurement")]
        public DateTime Date { get; set; }

        [Display(Name ="Vehicle speed")]
        public int VehicleSpeed { get; set; }
        
        [Display(Name = "Vehicle type")]
        public VehicleTypeModel VehicleType { get; set; }

        public TrafficSensorMeasurementModel (Traffic_Sensor_Measurment data)
        {
            Date = data._timestamp.DateTime;
            VehicleSpeed = data._vehicleSpeed;
            VehicleType = (VehicleTypeModel)(int)data._vehicleType;
        }
    }
}
