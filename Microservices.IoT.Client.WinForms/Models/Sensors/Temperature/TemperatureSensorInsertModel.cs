﻿using System;

namespace Microservices.IoT.Client.WinForms.Models.Sensors.Temperature
{
    public class TemperatureSensorInsertModel
    {
        public string Name { get; set; }
        public int TypeID { get; set; }
        public int ManufacturerID { get; set; }
        public DateTime LastTimeChecked { get; set; }
        public DateTime CreationDate { get; set; }
        public double Temperature { get; set; }
        public int OpenPortsCount { get; set; }
    }
}
