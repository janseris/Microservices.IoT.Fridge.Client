﻿using System;
using System.ComponentModel.DataAnnotations;

using Microservices.IoT.Sensors.Client.Temperature;

namespace Microservices.IoT.Client.WinForms.Models.Sensors.Temperature
{
    /// <summary>
    /// The name of the class had to be shortened when adding a DataSource for Grid in Designer 
    /// to prevent an error with no data source created (silent fail)
    /// because (probably) the file path was too long
    /// </summary>
    public class TemperatureSensorModel
    {
        [Display(Name = "ID")]
        public int TempSensorId { get; set; }
        public string Name { get; set; }

        #region sensor type

        [Display(Name = "Temperature sensor type ID")]
        public int TempSensorTypeId { get; set; }

        [Display(Name = "Type")]
        public string Type { get; set; }

        #endregion

        [Display(Name = "Open ports")]
        public int NumberOpenPorts { get; set; }

        [Display(Name = "Creation date")]
        public DateTime CreationDateTime { get; set; }

        [Display(Name = "Last time checked")]
        //[DisplayFormat(DataFormatString ="dd. MM. HH:mm:ss.fff")]
        public DateTime LastTimeChecked { get; set; }

        #region manufacturer

        [Display(Name = "Manufacturer ID")]
        public int TempSensorManufacturerId { get; set; }

        [Display(Name = "Manufacturer")]
        public string Manufacturer { get; set; }

        #endregion

        [Display(Name = "Temperature [°C]")]
        public double TempValue { get; set; }

        public TemperatureSensorModel(TempSensor data)
        {
            this.TempSensorId = data.TempSensorId;
            this.Name = data.Name;
            this.TempSensorTypeId = data.TempSensorTypeId;
            this.NumberOpenPorts = data.NumberOpenPorts;
            this.CreationDateTime = data.CreationDateTime.DateTime;
            this.LastTimeChecked = data.LastTimeChecked.DateTime;
            this.TempSensorManufacturerId = data.TempSensorManufacturerId;
            this.TempValue = data.TempValue;
        }
    }
}
