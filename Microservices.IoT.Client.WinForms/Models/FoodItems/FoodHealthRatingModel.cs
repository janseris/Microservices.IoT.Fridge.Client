﻿namespace Microservices.IoT.Client.WinForms.Models.FoodItems
{
    public class FoodHealthRatingModel
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public FoodHealthRatingModel(FoodHealthRating data)
        {
            this.ID = data.Id;
            this.Name = data.Name;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
