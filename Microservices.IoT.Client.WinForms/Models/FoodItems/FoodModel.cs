﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Microservices.IoT.Client.WinForms.Models.FoodItems
{
    public class FoodModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Producer { get; set; }
        public FoodTypeModel Type { get; set; }

        /// <summary>
        /// If the food item's packaging had been open
        /// </summary>
        public bool Open { get; set; }

        [Display(Name="Initial weight grams")]
        public int InitialWeightGrams { get; set; }

        /// <summary>
        /// Is reduced by consuming the food
        /// </summary>
        [Display(Name= "Current weight grams")]
        public int CurrentWeightGrams { get; set; }

        /// <summary>
        /// True if volume is not reduced when weight is reduced by consumption of a part of the food item
        /// </summary>
        [Display(Name= "Has immutable volume")]
        public bool HasImmutableVolume { get; set; }

        [Display(Name= "Date created")]
        public DateTime CreatedDate { get; set; }
        [Display(Name= "Expiration date")]
        public DateTime ExpirationDate { get; set; }

        public bool Expired => DateTime.Now > ExpirationDate;

        [Display(Name= "Current volume liters")]
        public double CurrentVolumeLiters => GetCurrentVolumeLiters();

        private double GetCurrentVolumeLiters()
        {
            if (HasImmutableVolume)
            {
                return InitialWeightGrams / Type.Density / 1000d;
            }
            else
            {
                return CurrentWeightGrams / Type.Density / 1000d;
            }
        }

        public FoodModel(Food data)
        {
            this.ID = data.Id;
            this.Name = data.Name;
            this.Producer = data.Producer;
            this.Type = new FoodTypeModel(data.Type);
            this.Open = data.Open;
            this.InitialWeightGrams = data.InitialWeightGrams;
            this.CurrentWeightGrams = data.CurrentWeightGrams;
            this.HasImmutableVolume= data.HasImmutableVolume;
            this.CreatedDate = data.CreatedDate.DateTime;
            this.ExpirationDate = data.ExpirationDate.DateTime;
        }
    }
}
