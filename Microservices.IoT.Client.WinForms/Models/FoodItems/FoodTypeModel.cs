﻿using System.ComponentModel.DataAnnotations;

namespace Microservices.IoT.Client.WinForms.Models.FoodItems
{
    public class FoodTypeModel
    {
        public int ID { get; set; }
        public string Name { get; set; }

        /// <summary>
        /// Weight in grams ratio to mililiters of volume
        /// <br>E.g. 2.0 = 2000 grams in 1000 mililiters</br>
        /// </summary>
        public double Density { get; set; }

        /// <summary>
        /// Suggested default value for <see cref="Food.HasImmutableVolume"/> when creating a food item of this type
        /// </summary>
        [Display(Name="Default value for immutable volume property")]
        public bool ImmutableVolumeHint { get; set; }

        [Display(Name = "Health rating")]
        public FoodHealthRatingModel HealthRating { get; set; }

        public FoodTypeModel(FoodType data)
        {
            this.ID = data.Id;
            this.Name = data.Name;
            this.Density = data.Density;
            this.ImmutableVolumeHint = data.ImmutableVolumeHint;
            this.HealthRating = new FoodHealthRatingModel(data.HealthRating);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
