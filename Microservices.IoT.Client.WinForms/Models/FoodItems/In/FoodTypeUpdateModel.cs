﻿namespace Microservices.IoT.Client.WinForms.Models.FoodItems.In
{
    public class FoodTypeUpdateModel
    {
        /// <summary>
        /// Density defines the volume of the food item calculated from its weight
        /// <br><c>kg</c>/<c>liter</c> or <c>grams</c>/<c>mililiter</c></br>
        /// </summary>
        public double Density { get; set; }
        public int HealthRatingID { get; set; }

        /// <summary>
        /// Immutable volume signifies if the volume of the food item changes if the weight is reduced when a part of the item is consumed
        /// </summary>
        public bool ImmutableVolumeHint { get; set; }

        public FoodTypeUpdateDTO Map()
        {
            return new FoodTypeUpdateDTO
            {
                Density = Density,
                HealthRatingID = HealthRatingID,
                ImmutableVolumeHint = ImmutableVolumeHint
            };
        }
    }
}
