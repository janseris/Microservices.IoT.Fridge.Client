﻿namespace Microservices.IoT.Client.WinForms.Models.FoodItems.In
{
    public class FoodTypeCreateModel
    {
        public string Name { get; set; }
        public double Density { get; set; }
        public bool ImmutableVolumeHint { get; set; }
        public int HealthRatingID { get; set; }

        public FoodTypeCreateDTO Map()
        {
            return new FoodTypeCreateDTO
            {
                Name = Name,
                Density = Density,
                ImmutableVolumeHint = ImmutableVolumeHint,
                HealthRatingID = HealthRatingID
            };
        }
    }
}
