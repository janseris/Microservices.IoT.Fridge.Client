﻿using System;

namespace Microservices.IoT.Client.WinForms.Models.FoodItems.In
{
    public class FoodCreateModel
    {
        public string Name { get; set; }
        public string Producer { get; set; }
        public int TypeID { get; set; }
        public bool Open { get; set; }
        public int WeightGrams { get; set; }
        public bool ImmutableVolume { get; set; }
        public DateTime ExpirationDate { get; set; }

        public FoodCreateDTO Map()
        {
            return new FoodCreateDTO
            {
                Name = Name,
                Producer = Producer,
                TypeID = TypeID,
                Open = Open,
                WeightGrams = WeightGrams,
                ImmutableVolume = ImmutableVolume
            };
        }
    }
}
