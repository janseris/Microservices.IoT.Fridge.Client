﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Xml.Linq;

using Microservices.IoT.Client.WinForms.Models.Events;
using Microservices.IoT.Client.WinForms.Models.FoodItems;
using Microservices.IoT.Client.WinForms.Models.Fridges.Info;

namespace Microservices.IoT.Client.WinForms.Models.Fridges
{
    public class FridgeModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
        public FridgeTypeModel Type { get; set; }
        public List<FoodModel> Food { get; set; }
        public List<EventModel> Events { get; set; }
        public FridgeInfoModel Info { get; set; }

        [Display(Name = "Occupied volume [Liters]")]
        public double OccupiedVolumeLiters => Food.Sum(item => item.CurrentVolumeLiters);
        
        [Display(Name = "Occupied volume [%]")]
        public double OccupiedVolumePercent => OccupiedVolumeLiters / Info.Static.CapacityLiters * 100d;
        
        [Display(Name = "Free volume [%]")]
        public double FreeVolumePercent => 100d - OccupiedVolumeLiters;
        
        [Display(Name = "Free volume [Liters]")]
        public double FreeVolumeLiters => Info.Static.CapacityLiters - OccupiedVolumeLiters;

        public FridgeModel(Fridge data)
        {
            this.ID = data.Id;
            this.Name = data.Name; 
            this.Brand = data.Brand;
            this.Type = new FridgeTypeModel(data.Type);
            this.Food = data.Food.Select(item => new FoodModel(item)).ToList();
            this.Events = data.Events.Select(item => new EventModel(item)).ToList();
            this.Info = new FridgeInfoModel(data.Info);
        }
    }
}
