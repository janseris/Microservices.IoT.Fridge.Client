﻿namespace Microservices.IoT.Client.WinForms.Models.Fridges.In
{
    public class FridgeCreateModel
    {
        public string Name { get; set; }
        public string Brand { get; set; }
        public int TypeID { get; set; }
        public int DesignedMaximalPowerConsumptionWatts { get; set; }
        public int CapacityLiters { get; set; }
        public double ConfiguredOperatingTemperatureDegrees { get; set; }
        public int DoorOpenPeriodSecondsForWarning { get; set; }

        /// <summary>
        /// By how many degrees the <see cref="ConfiguredOperatingTemperatureDegrees"/> can be exceeded to generate a "high temperature" event
        /// </summary>
        public double DegreesOffsetForTemperatureWarning { get; set; }

        public FridgeCreateDTO Map()
        {
            return new FridgeCreateDTO()
            {
                Name = Name,
                Brand = Brand,
                TypeID = TypeID,
                DesignedMaximalPowerConsumptionWatts = DesignedMaximalPowerConsumptionWatts,
                CapacityLiters = CapacityLiters,
                ConfiguredOperatingTemperatureDegrees = ConfiguredOperatingTemperatureDegrees,
                DoorOpenPeriodSecondsForWarning = DoorOpenPeriodSecondsForWarning,
                DegreesOffsetForTemperatureWarning = DegreesOffsetForTemperatureWarning
            };
        }
    }
}
