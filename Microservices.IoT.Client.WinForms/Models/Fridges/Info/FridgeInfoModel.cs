﻿using System.ComponentModel.DataAnnotations;

namespace Microservices.IoT.Client.WinForms.Models.Fridges.Info
{
    public class FridgeInfoModel
    {
        [Display(Name = "Static info")]
        public FridgeStaticInfoModel Static { get; set; }
        
        [Display(Name = "Configurable info")]
        public FridgeConfigurableInfoModel Configurable { get; set; }
        
        [Display(Name = "Info from sensors")]
        public FridgeSensorInfoModel Sensors { get; set; }

        public FridgeInfoModel(FridgeInfo data)
        {
            this.Static = new FridgeStaticInfoModel(data.Static);
            this.Configurable = new FridgeConfigurableInfoModel(data.Configurable);
            this.Sensors = new FridgeSensorInfoModel(data.Sensors);
        }
    }
}
