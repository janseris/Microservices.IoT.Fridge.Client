﻿using System.ComponentModel.DataAnnotations;

namespace Microservices.IoT.Client.WinForms.Models.Fridges.Info
{
    public class FridgeConfigurableInfoModel
    {

        [Display(Name = "Configured operating temperature [°C]")]
        public double ConfiguredOperatingTemperatureDegrees { get; set; }

        [Display(Name = "Door open period for warning [seconds]")]
        public int DoorOpenPeriodSecondsForWarning { get; set; }

        [Display(Name = "Ooffset for temperature warning [°C]")]
        public double DegreesOffsetForTemperatureWarning { get; set; }

        public FridgeConfigurableInfoModel(FridgeConfigurableInfo data)
        {
            this.ConfiguredOperatingTemperatureDegrees = data.ConfiguredOperatingTemperatureDegrees;
            this.DoorOpenPeriodSecondsForWarning = data.DoorOpenPeriodSecondsForWarning;
            this.DegreesOffsetForTemperatureWarning = data.DegreesOffsetForTemperatureWarning;
        }
    }
}
