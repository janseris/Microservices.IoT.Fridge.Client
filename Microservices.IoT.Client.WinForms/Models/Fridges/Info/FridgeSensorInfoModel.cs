﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace Microservices.IoT.Client.WinForms.Models.Fridges.Info
{
    public class FridgeSensorInfoModel
    {
        [Display(Name = "Temperature [°C]")]
        public double TemperatureDegrees { get; set; }

        [Display(Name = "Power consumption [W]")]
        public int PowerConsumptionWatts { get; set; }
        
        [Display(Name = "Is open")]
        public bool IsOpen { get; set; }

        public FridgeSensorInfoModel(FridgeSensorInfo data)
        {
            TemperatureDegrees = data.TemperatureDegrees;
            PowerConsumptionWatts= data.PowerConsumptionWatts;
            IsOpen = data.IsOpen;
        }
    }
}
