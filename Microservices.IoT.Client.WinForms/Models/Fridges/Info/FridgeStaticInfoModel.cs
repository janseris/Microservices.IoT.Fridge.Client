﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace Microservices.IoT.Client.WinForms.Models.Fridges.Info
{
    public class FridgeStaticInfoModel
    {
        [Display(Name = "Designed maximal power consumption [Watts]")]
        public int DesignedMaximalPowerConsumptionWatts { get; set; }
        
        [Display(Name = "Capacity [Liters]")]
        public int CapacityLiters { get; set; }
        
        public FridgeStaticInfoModel(FridgeStaticInfo data) 
        {
            this.DesignedMaximalPowerConsumptionWatts = data.DesignedMaximalPowerConsumptionWatts;
            this.CapacityLiters = data.CapacityLiters;
        }
    }
}
