﻿namespace Microservices.IoT.Client.WinForms.Models.Fridges
{
    public class FridgeTypeModel
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public FridgeTypeModel(FridgeType data)
        {
            this.ID = data.Id;
            this.Name = data.Name;
        }
    }
}
