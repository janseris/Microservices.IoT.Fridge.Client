﻿namespace Microservices.IoT.Client.WinForms.Models.Events
{
    public class EventTypeModel
    {
        public EventTypeModel(EventType data)
        {
            this.ID = data.Id;
            this.Name = data.Name;
        }

        public int ID { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}

