﻿using System;

namespace Microservices.IoT.Client.WinForms.Models.Events
{
    public class EventModel
    {
        public int ID { get; set; }
        public EventTypeModel Type { get; set; }

        /// <summary>
        /// Automatically filled when the event starts
        /// </summary>
        public DateTime From { get; set; }

        /// <summary>
        /// Filled when the event ends
        /// </summary>
        public DateTime? To { get; set; }

        public EventModel(Event data)
        {
            this.ID = data.Id;
            this.Type = new EventTypeModel(data.Type);
            this.From = data.From.DateTime;
            this.To = data.To?.DateTime;
        }

        public override string ToString()
        {
            return Type.ToString();
        }
    }
}
